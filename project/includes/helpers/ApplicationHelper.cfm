
<!--- All methods in this helper will be available in all handlers,plugins,views & layouts --->


	<!---stringIfFind()--->
	<cffunction name="navSub" output="false" access="public" returntype="any" hint="return html for navigation sub menu">
		<cfargument name="label" required="true">
		<cfargument name="link" required="true">
		<cfargument name="linkevent" required="true">
		<cfargument name="event" required="true">
		<cfset event = arguments.event>
		<cfsavecontent variable="navitem">
			<cfoutput>
				<li><a class="#stringIf('active',event.getCurrentEvent(),arguments.linkevent)#" href="#event.buildLink(link,false)#">#label#</a></li>
			</cfoutput>
		</cfsavecontent>
	
		<cfreturn navitem>
		
		
		
	</cffunction>

	<!---stringIf()--->
	<cffunction name="stringIf" output="false" access="public" returntype="any" hint="display string1 if condition1 = condition2 , else display optional string2">
		<cfargument name="string1" required="false" default="">
		<cfargument name="condition1" required="false" default="">
		<cfargument name="condition2" required="false" default="">
		<cfargument name="string2" required="false" default="">
		<cfscript>
			for(var i=1; i<=listLen(arguments.condition2); i++){
				if (condition1 EQ listGetAt(condition2,i))
					return string1;
				else if (len(trim(string2)))
					return string2;
			}
		</cfscript>
	</cffunction>
	
	<!---stringIfFind()--->
	<cffunction name="stringIfFind" output="false" access="public" returntype="any" hint="display string1 if condition1 can be found in condition2 using findNoCase, else display opitional string2">
		<cfargument name="string1" required="false" default="">
		<cfargument name="condition1" required="false" default="">
		<cfargument name="condition2" required="false" default="">
		<cfargument name="string2" required="false" default="">
		<cfscript>
			for(var i=1; i<=listLen(arguments.condition2); i++){
				if (findNoCase(listGetAt(condition2,i),condition1))
					return string1;
				else if (len(trim(string2)))
					return string2;
			}
				
		</cfscript>
	</cffunction>
	
	