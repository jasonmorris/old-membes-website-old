
$(document).ready(function($) {

	//destroy modal once hidden so that it reloads if opened again
	$('body').on('hidden.bs.modal', '.modal', function () {
	    $(this).removeData('bs.modal');
		$("#modal").html('<div class="modal-dialog"><div class="modal-content"><div class="modal-body" style="height:250px;"> LOADING... </div></div></div>');
	});

}); 