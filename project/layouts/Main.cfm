﻿<cfoutput>

<!doctype html>

<html lang="en" class="no-js">
<head>

	<cfparam name="rc.pageTitle" default="Association Software - membes">
	<cfparam name="rc.metaDescription" default="Association Management Software for Australian Associations.  Website, Database, Events, Communications, Payments & more in one seamlessly integrated system.">
	<cfparam name="rc.metaKeywords" default="Membership Software, Assocation Software, Association Management Software, Association Website, Membership Database">

	<title>#rc.pageTitle#</title>
	<meta name="description" content="#rc.metaDescription#">
	<meta name="keywords" content="#rc.metaKeywords#">

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>

    <!------>
    <link href='https://fonts.googleapis.com/css?family=Lato:100,400' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>


    <script type="text/javascript" src="//use.typekit.net/cvk5vpv.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>


	<!---<link rel="stylesheet" type="text/css" href="/includes/css/bootstrap.css" media="screen">--->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">


    <!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="/includes/css/fullwidth.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/includes/css/settings.css" media="screen" />

	<link rel="stylesheet" type="text/css" href="/includes/css/magnific-popup.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/includes/css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/includes/css/owl.theme.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/includes/css/jquery.bxslider.css" media="screen">

    <!---
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
	--->
	<link rel="stylesheet" type="text/css" href="/includes/css/font-awesome/fontawesome-all.min.css" media="screen">

	<!---<link rel="stylesheet" type="text/css" href="/includes/css/font-awesome.css" media="screen">--->
	<link rel="stylesheet" type="text/css" href="/includes/css/animate.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/includes/css/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/includes/css/responsive.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/includes/css/custom.css" media="screen">

	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	

</head>
<body>

	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
		<header class="clearfix header2">
			<!-- Static navbar -->
			<div class="navbar navbar-default navbar-fixed-top">
		
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="/" id="header-logo">
                        	membes
                        </a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">

							<li><a class="#stringIfFind('active',event.getCurrentEvent(),'main')#" href="/"><span></span>Home</a></li>
                            
                            <li class="drop"><a class="#stringIfFind('active',event.getCurrentEvent(),'benefits.')#" href="##" onclick="return false;"><span></span>Benefits</a>
								<ul class="drop-down">

                                    #navSub('Membership','benefits/membership/','benefits.membership',event)#
                                    #navSub('Events','benefits/events/','benefits.events',event)#
                                    #navSub('Finance','benefits/finance/','benefits.finance',event)#
                                    #navSub('Communication & Engagement','benefits/communication-and-engagement/','benefits.communication-and-engagement',event)#
                                    #navSub('Education & CPD','benefits/education-and-cpd/','benefits.education-and-cpd',event)#
                                    #navSub('Membership Growth','benefits/membership-growth/','benefits.membership-growth',event)#
									#navSub('Executive & Board','benefits/executive-and-board/','benefits.executive-and-board',event)#
									#navSub('Latest technology standards','benefits/latest-technology-standards','benefits.latest-technology-standards',event)#

									<!---
                                    #navSub('Benefits for the Board','benefits/for-the-board/','benefits.for-the-board',event)#
                                    #navSub('Benefits for Executives','benefits/for-executives/','benefits.for-executives',event)#
                                    #navSub('Benefits for Staff','benefits/for-staff/','benefits.for-staff',event)#
									--->

                                </ul>
                            </li>
                            
                            
							<li class="drop"><a class="#stringIfFind('active',event.getCurrentEvent(),'about.')#" href="##" onclick="return false;"><span></span>About</a>
								<ul class="drop-down">
                                	
                                    #navSub('Membership Software Features','about/membership-software/','about.membership-software',event)#
                                    
                                    #navSub('90 Second Overview (video)','about/explainer-video/','about.explainer-video',event)#
                                    #navSub('Technical Overview','about/technical-overview/','about.technical-overview',event)#
									#navSub('Mobile App','about/mobileapp/','about.mobileapp',event)#
                                    #navSub('Who is Using membes','about/who-is-using-membes/','about.who-is-using-membes',event)#
                                    

                                    <!---#navSub('Setup Process','about/setup/','about.setup',event)#--->

								</ul>
							</li>
                            
                            
                            <!---
                            <li><a class="#stringIfFind('active',event.getCurrentEvent(),'benefits')#" href="#event.buildLink('benefits/')#"><span></span>Benefits</a></li>
                           --->
                            
							<li><a class="#stringIfFind('active',event.getCurrentEvent(),'demo')#" href="#event.buildLink('demo/')#"><span></span>Request Demo </a></li>
                            
							<li><a class="#stringIfFind('active',event.getCurrentEvent(),'contact')#" href="#event.buildLink('contact/')#"><span></span>Contact</a></li>
							
						</ul>
					</div>
				</div>
			</div>
		</header>
		<!-- End Header -->

		<!-- content 
			================================================== -->
		<div id="content">
			
			#renderView()#
			

		</div>
		<!-- End content -->


		<!-- footer 
			================================================== -->
		<footer>
		
        	<div class="up-footer">
				<div class="container">
					<div class="row">

						<div class="col-md-5 triggerAnimation animated fadeInLeft" data-animate="fadeInLeft" style="">
							<div class="widget footer-widgets text-widget">
								<p id="footer-logo">membes</p>
								<p>Association Management Software for Australian Associations.</p>
							</div>
						</div>

						<div class="col-md-12">
                        	
                        </div>

					</div>
				</div>
			</div>
        
			<div class="footer-line">
				<div class="container">
					<p>&##169; #year(now())# asset media pty ltd,  all rights reserved</p>
					<ul class="footer-social-icons">
						<ul class="social-icons">
							<!---
							<li><a class="facebook" href="##"><i class="fa fa-facebook"></i></a></li>
							<li><a class="twitter" href="##"><i class="fa fa-twitter"></i></a></li>
							--->
							<li><a class="linkedin" href="#event.buildLink('contact/')#"><i class="fa fa-envelope"></i></a></li>
                            <li><a class="linkedin" href="#event.buildLink('contact/')#"><i class="fa fa-phone"></i></a></li>
						</ul>					
					</ul>
				</div>
			</div>

		</footer>
		<!-- End footer -->
	</div>
	<!-- End Container -->

	<div id="modal" class="modal fade"></div>

	<script type="text/javascript" src="/includes/js/jquery.min.js"></script>
	<script type="text/javascript" src="/includes/js/jquery.migrate.js"></script>
	<script type="text/javascript" src="/includes/js/jquery.magnific-popup.min.js"></script>

	<script type="text/javascript" src="/includes/js/bootstrap.js"></script>
	<!---<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>--->

	<script type="text/javascript" src="/includes/js/owl.carousel.min.js"></script>
	<!---<script type="text/javascript" src="/includes/js/retina-1.1.0.min.js"></script>--->
	<script type="text/javascript" src="/includes/js/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="/includes/js/plugins-scroll.js"></script>
	<script type="text/javascript" src="/includes/js/waypoint.min.js"></script>

     <!-- jQuery KenBurn Slider  -->
    <script type="text/javascript" src="/includes/js/jquery.themepunch.revolution.min.js"></script>
	<script type="text/javascript" src="/includes/js/script.js"></script>
	<script type="text/javascript" src="/includes/js/custom.js"></script>




	<script type="text/javascript">

		var tpj=jQuery;
		tpj.noConflict();

		tpj(document).ready(function() {

		if (tpj.fn.cssOriginal!=undefined)
			tpj.fn.css = tpj.fn.cssOriginal;

			var api = tpj('.fullwidthbanner').revolution(
				{
					delay:8000,
					startwidth:1170,
					startheight:500,

					onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off

					thumbWidth:100,							// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
					thumbHeight:50,
					thumbAmount:3,

					hideThumbs:0,
					navigationType:"none",				// bullet, thumb, none
					navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none

					navigationStyle:"round",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom


					navigationHAlign:"center",				// Vertical Align top,center,bottom
					navigationVAlign:"bottom",					// Horizontal Align left,center,right
					navigationHOffset:30,
					navigationVOffset: 40,

					soloArrowLeftHalign:"left",
					soloArrowLeftValign:"center",
					soloArrowLeftHOffset:0,
					soloArrowLeftVOffset:0,

					soloArrowRightHalign:"right",
					soloArrowRightValign:"center",
					soloArrowRightHOffset:0,
					soloArrowRightVOffset:0,

					touchenabled:"on",						// Enable Swipe Function : on/off


					stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
					stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic

					hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
					hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
					hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value


					fullWidth:"on",

					shadow:1								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)

				});


				// TO HIDE THE ARROWS SEPERATLY FROM THE BULLETS, SOME TRICK HERE:
				// YOU CAN REMOVE IT FROM HERE TILL THE END OF THIS SECTION IF YOU DONT NEED THIS !
					api.bind("revolution.slide.onloaded",function (e) {


						jQuery('.tparrows').each(function() {
							var arrows=jQuery(this);

							var timer = setInterval(function() {

								if (arrows.css('opacity') == 1 && !jQuery('.tp-simpleresponsive').hasClass("mouseisover"))
								  arrows.fadeOut(300);
							},3000);
						})

						jQuery('.tp-simpleresponsive, .tparrows').hover(function() {
							jQuery('.tp-simpleresponsive').addClass("mouseisover");
							jQuery('body').find('.tparrows').each(function() {
								jQuery(this).fadeIn(300);
							});
						}, function() {
							if (!jQuery(this).hasClass("tparrows"))
								jQuery('.tp-simpleresponsive').removeClass("mouseisover");
						})
					});
				// END OF THE SECTION, HIDE MY ARROWS SEPERATLY FROM THE BULLETS
			});
	</script>
	
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	  ga('create', 'UA-769732-34', 'auto');
	  ga('send', 'pageview');
	</script>

	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 878391255;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/878391255/?guid=ON&amp;script=0"/>
	</div>
	</noscript>

	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '276349272786483'); // Insert your pixel ID here.
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=276349272786483&ev=PageView&noscript=1"
	/></noscript>
	<!-- DO NOT MODIFY -->
	<!-- End Facebook Pixel Code -->
	
	<!-- Drip -->
	<script type="text/javascript">
	  var _dcq = _dcq || [];
	  var _dcs = _dcs || {};
	  _dcs.account = '5041188';

	  (function() {
		var dc = document.createElement('script');
		dc.type = 'text/javascript'; dc.async = true;
		dc.src = '//tag.getdrip.com/5041188.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(dc, s);
	  })();
	</script>
	<!-- end Drip -->
</body>
</html>


	
	
</cfoutput>