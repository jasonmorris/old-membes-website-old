<cfoutput>

<!doctype html>

<html lang="en" class="no-js">
<head>

	<title></title>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!---<link rel="stylesheet" type="text/css" href="/includes/css/bootstrap.css" media="screen">--->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">


    <!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="/includes/css/fullwidth.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/includes/css/settings.css" media="screen" />

	<link rel="stylesheet" type="text/css" href="/includes/css/magnific-popup.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/includes/css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/includes/css/owl.theme.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/includes/css/jquery.bxslider.css" media="screen">

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

	<!---<link rel="stylesheet" type="text/css" href="/includes/css/font-awesome.css" media="screen">--->
	<link rel="stylesheet" type="text/css" href="/includes/css/animate.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/includes/css/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/includes/css/responsive.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/includes/css/custom.css" media="screen">

	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />


</head>
<body>


		<div id="content">

			#renderView()#

		</div>

</body>
</html>




</cfoutput>