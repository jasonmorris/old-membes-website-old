
<cfscript>
	param name="rc.modalWidth" default="";
	modalClass = "";
	if (rc.modalWidth is 'wide')
		modalClass = " modal-lg";
</cfscript>


<cfoutput>

<!doctype html>

<html lang="en" class="no-js">
<head>

	<cfparam name="rc.pageTitle" default="Feature Overview">
	<title>#rc.pageTitle#</title>
	<meta name="description" content="Association Management Software for Australian Associations.  Website, Database, Events, Communications, Payments & more in one seamlessly integrated system.">
	<meta name="keywords" content="Assocation Software, Association Management Software, Association Website, Association Database">

</head>
<body>

	<div class="modal-dialog#modalClass#">
		<div class="modal-content">
			
			<cfif structKeyExists(rc,'modalHeading')>
				<div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				    <h1>#rc.modalHeading#</h1>
				</div>
			</cfif>
			
			<div class="modal-body">
				#renderView()#
			</div>

		</div>
	</div>

</body>
<html>
</cfoutput>