component {
	
	function configure(){
		
		coldbox = {
			appName = "MembesWebsite",
            defaultEvent = "home.main",
			exceptionHandler		= "errors.error404",
			onInvalidEvent			= "errors.error404",
            UDFLibraryFile = "includes/helpers/ApplicationHelper.cfm",
			reinitPassword = '',
			
			handlerCaching 			= true,
			eventCaching			= true
			
		};
		
		flash = {
			scope = "session",
			properties = {}, // constructor properties for the flash scope implementation
			inflateToRC = true, // automatically inflate flash data into the RC scope
			inflateToPRC = false, // automatically inflate flash data into the PRC scope
			autoPurge = false, // automatically purge flash data for you
			autoSave = true // automatically save flash scopes at end of a request and on relocations.
		};

		interceptors = [
			{class="coldbox.system.interceptors.SES", properties={}}
		];

		layouts = [
			{ name = "Modal",
		 	  file = "Modal.cfm",
			  folders = "featureoverview"
			}
		];
		
		
		
	}

}