﻿<cfscript>
	
	// Base URL
	if( len(getSetting('AppMapping') ) lte 1){
		setBaseURL("http://#cgi.HTTP_HOST#");
	}
	else{
		setBaseURL("http://#cgi.HTTP_HOST#/#getSetting('AppMapping')#");
	}


	addRoute(pattern="about/explainer-video", handler="about", action="explainervideo");
	addRoute(pattern="about/membership-software", handler="about", action="features");
	addRoute(pattern="about/technical-overview", handler="about", action="technicaloverview");
	addRoute(pattern="about/mobileapp", handler="about", action="mobileapp");
    
	addRoute(pattern="info/the-difference-between-an-ams-and-a-crm", handler="info", action="article1");
	addRoute(pattern="info/bespoke-vs-saas-association-management-software", handler="info", action="article2");
	addRoute(pattern="info/open-over-closed-ecosystem-association-software", handler="info", action="article3");
	addRoute(pattern="info/maximum-design-flexibility", handler="info", action="article4");
	addRoute(pattern="info/association-software-pitfalls", handler="info", action="article5");
	addRoute(pattern="info/engage-with-your-members", handler="info", action="article6");
	
	
	
	addRoute(pattern=":handler/:action?");
	
</cfscript>