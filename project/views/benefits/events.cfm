<cfoutput>

<div class="page-banner page-banner-narrow page-banner-crowd-blue">
	<h2>Improved Event Management </h2>
</div>

<div class="page-intro">
    <div class="title-section">
        <div class="container">

            #renderView('_includes/benefits_nav')#

        	<div class="text-holder">
                <h1>Event Management and Integration</h1>
                <p><b>Critical as both a regular revenue stream and growth generator for an Association, effective and integrated event management is essential to a progressive, Professional Association.</b></p>

                <p>
                    Events play a vital part in providing value to your members, as well as creating incentives for prospective members to Join.  It's also often the biggest time consumer for your staff and volunteers.
                    Integrated event management tools and processes ensure you can deliver maximum value to your Stakeholders while reducing the Administrative and Management burden often associated with setting up and managing your events.
                </p>

                <table border="0" width="100%" class="infotable">

                    <thead>
                        <tr>
                            <th colspan="3">Membership Management</th>
                        </tr>
                    </thead>

				    <tbody>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="far fa-calendar-alt fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Integrated and easy to navigate Event Calendars</h3>
                                <p>
                                    Simple or Complex events can easily be configured and published to public or private calendars.
                                    Event teasers easily pushed to your websites home page, which links through to chronological list to then be filtered by date, state and more.
                                    Event listings can be kept very simple, or even include a dedicated information page containing programs, videos are anything needed to effectively promote your event through your website.
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-user-cog fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Simple Event Registration processes tightly integrated with your Stakeholders Profiles and Processes</h3>
                                <p>
                                    The Event registration process is a purpose built process.
                                    The user only needs to enter their email address and if they are already in your database (as a member or other stakeholder), the system will link their registration directly to that profile.
                                    If the user does not have a profile the system will create one as part of the registration process.
                                    All the user then needs to do is select the options specific to them, enter their credit card details (if payment required) and their payment is processed, and an email sent containing a ticket with their registration details and an invoice.
                                    All registrations are recorded against the users profile so you can always see a full history of events attended.
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-hand-holding-usd fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Easily setup different prices for different Membership and Stakeholder types and Discounted pricing</h3>
                                <p>
                                    Because membes is an integrated Single Point of Truth system, it is very easy to set price points for your different Membership and Stakeholder types.
                                    Event Pricing options can either be setup to be linked directly to a Membership Type (and only that membeship type can view that pricing).  For example, setup individual pricing for Full Members, Student Members etc..
                                    Or you can setup pricing Options to be be selected by non members, such as Speakers, Trade Delegates etc..
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-cubes fa-5x green"></i>
                            </td>
                            <td>
                                <h3>From basic Board Meetings to more complex multi-day, multi session-workshop Events and Conferences</h3>
                                <p>
                                    Within the one Events Management System, easily setup simple Events or Meetings that do not require a registration, all the way through to Multi Day, Multi Session Events.
                                    All events start out the same when first created, but a lot of elements can be added to an event such as Sessions (e.g. Friday, Saturday, Full.. Morning, Afternoon etc.), Add Ons (e.g. Welcome Drinks, Social Evening, VIP Seating etc.) through to
                                    Workshops (or break out meetings) which can also be separated into Workshop Streams.
                                    The membes integrated Events module has the ability to handle everything from a local Branch meeting through to your Annual National Conference.
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-calendar-check fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Powerful Event Management capabilities that significantly reduces the workload around managing your events.</h3>
                                <p>
                                    Managing the event (both before and after the actual event) so that it is a success can be a big task.
                                    membes comes with a range of tools and automated processes that massively reduces the time needed to carry out key tasks.<br>
                                    <em>Before the Event:</em> Easily view or export delegate list.  Who has paid or pending payment.  Send emails to stakeholders that have or have not yet registered for the event and more.<br>
                                    <em>After the Event:</em> Batch (with one click) send Certificates of Attendance, update all members CPD logs with Activity Type and Points, flag attended or not and more.
                                </p>
                            </td>
                        </tr>

                         <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-university fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Integrated CPD Module for simple one click recording of all attendees CPD Activity Log</h3>
                                <p>
                                    The membes Events module integrates seamlessly with the membes CPD Module.
                                    When setting up an event, you can specify which CPD Activity Type this event is associated with, as well as how many CPD points an attendant is eligible for (even down to which event sections or workshops they attended).
                                    After the event, within a few minutes, you can then batch record CPD Activity Logs for that event for everyone that attended that event.
                                    This not only saves hours of work by yourself or members, but it also offers significant value to your members and provides a considerable Engagement and Incentive to current and prospective members.
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-tv fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Setup Conference Micro-Sites right within the membes CMS without need to engage an external developer.</h3>
                                <p>
                                    Traditionally when you run a conference you will either engage a web developer (or pay a PCO) to setup a dedicated website for that conference.
                                    The membes Micro-sites module allows you to very easily setup and maintain a dedicated website for your conference for a FRACTION of the cost of engaging a web developer.
                                    Because the Micro-site is integrated with your main websites back end, it allows for your Micro Site to continue integrating with and leveraging the benefits of a Single Point of Truth system.
                                </p>
                            </td>
                        </tr>





                    </tbody>
                </table>

            </div>

         </div>
    </div>
</div>


<div class="tempcore-line">
    <div class="container">
        <a href="/demo/" style="width: 220px">Request FREE Demo</a>
        <p><i class="fa fa-thumbs-up"></i>Future Proof your Association with current best practice in technologies and standards.</p>
    </div>
</div>


</cfoutput>   