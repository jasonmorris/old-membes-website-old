<cfoutput>

<div class="page-banner page-banner-narrow page-banner-crowd-blue">
	<h2>Improve education opportunities and CPD processes</h2>
</div>

<div class="page-intro">
    <div class="title-section">
        <div class="container">

             #renderView('_includes/benefits_nav')#

        	<div class="text-holder">
                <h1>Education and CPD</h1>
                <p><b>Provide Continued Professional Development opportunities and Logging/Auditing capabilities in a way your staff and members are going to love</b></p>
                <p>
                    Education is a big part of a Professional Associations offering to it's members and incentive to Join for it's Prospective Members.
                    Members provides a range of ways that this effort can be enhanced through 3rd party integration with leading Learning Management Systems and a simple to use yet extremely effective CPD Logging and Auditing module.
                </p>

                <table border="0" width="100%" class="infotable">

                    <thead>
                        <tr>
                            <th colspan="3">Education and CPD </th>
                        </tr>
                    </thead>

				    <tbody>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-graduation-cap fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Feature Rich CPD Tracking and Management module for Members and Administrators</h3>
                                <p>
                                    Integrated with membes is a CPD Module which allows members (and/or administrators on a members behalf) log CPD Activities.
                                    These activities can then viewed and reported on by CPD Cycle, Activity Types and Categories.
                                    Members can see at a glance how many "Eligible" points/hours they have earned for the current (or past) cycles, how many points/hours they still need to earn to achieve this cycles required points/hours, how many by activity type and category and more.
                                    The membes CPD Module is much more than just a log, but a tool that allows all stakeholders easily manage and know at a glance where they are with their CPD Activities and targets... and much more ...
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-sliders-h fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Highly configurable CPD Activity Cycles, Streams and Types</h3>
                                <p>
                                    CPD Programs have varying rules, categorisations and targets.
                                    The membes CPD Module allows for configuration in accordance with your programs rules.
                                    CPD Cycles can be setup as required, or even added to Cycle streams for complex cycle requirements or parallel cycles to be available.
                                    Activity types can be setup with type specific caps (so only a certain number of points/hours for that Activity Type are calculated towards the Cycle target).
                                    Automatic calculations can be setup so that an accurate picture of a members CPD status can be seen with ease and without error.
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-cogs fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Module integration which allows Automated logging of CPD Activities Complete and CPD Events Attended</h3>
                                <p>
                                    Because membes is a Single Point of Truth system, there is tight integration between the membes CPD module and other modules.
                                    For example, when setting up an Event or Course in the Events Calendar, you can link that event to an Activity Type (e.g. Online Learing), and also specify how many points/hours an attendee is eligible if they attend that event.
                                    After the event is complete, with a few clicks ALL attendees (whether it is 1 or 1,000) can have the activity automatically written to each of their CPD Logs, allocated to the appropriate Activity Type with the pre-determined number of CPD Points/Hours.
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <img src="/includes/img/logo/intuto.png" width="90px">
                            </td>
                            <td>
                                <h3>Two way integration with the Intuto Learning Management System </h3>
                                <p>
                                    membes has been integrated with the Intuto Learning Management System.
                                    This allows you to run your training through the Intuto Learning Management System, see what courses have been done by your members and more.
                                    When your members have completed a course within the Intuto LMS, a CPD record can then automatically be passed from Intuto in the membes CPD module to record that activity against a members CPD Log.
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-tasks fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Comprehensive CPD Log Approval and Auditing tools for Administration </h3>
                                <p>
                                    The CPD module contains a range of tools to facilitate Approval and Auditing of CPD Logs submitted.
                                    Activity Types can be configured so that any Logs submitted to that Activity Type go into a "Pending Approval" holding bay where Administration can easiliy review and approve the Activity Log before it is moved into the members CPD Log.
                                    There are also Batch Auditing tools where a percentage of your members can be passed through a large scale Audit process, allowing online and automated requests for supporting documentation, reviewing and approval of individual logs and more.
                                </p>
                            </td>
                        </tr>





                    </tbody>
                </table>


            </div>



         </div>
    </div>
</div>


<div class="tempcore-line">
    <div class="container">
        <a href="/demo/" style="width: 220px">Request FREE Demo</a>
        <p><i class="fa fa-thumbs-up"></i>Future Proof your Association with current best practice in technologies and standards.</p>
    </div>
</div>


</cfoutput>