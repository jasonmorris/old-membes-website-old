<cfoutput>

<div class="page-banner page-banner-narrow page-banner-crowd-blue">
	<h2>Improve administrative quality and efficiencies</h2>
</div>


<div class="page-intro">
    <div class="title-section">
        <div class="container">

            #renderView('_includes/benefits_nav')#

        	<div class="text-holder">
                <h1>Membership</h1>
                <p><b>The core of the membes Association Management System is an Enterprise grade Membership Management System.</b></p>

                <p>Providing key capabilities critical for a progressive Association to achieve it's strategic objectives, in areas such as Single point of Truth Data Capturing, Automated workflows, Integration, Self Servicing and more. </p>

                <table border="0" width="100%" class="infotable">

                    <thead>
                        <tr>
                            <th colspan="3">Membership Management</th>
                        </tr>
                    </thead>

				    <tbody>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-database fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Centralized, Single Point of Truth Data to maximise Efficiencies and Insights</h3>
                                <p>
                                    All Stakeholder (Members, Prospective members etc.) data is stored in one central location.
                                    This ensures that from one central place you can manage all aspects of managing your members and all members activities across all aspects of your organisation are processed and captured in that one central place.
                                    This centralisation of data and activities significantly reduces complexity while increasing quality in relation to hour your members are managed, communicated to, reported on, engaged and grown.
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-cogs fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Fully automated, online and easy to use Joining & Renewal processes</h3>
                                <p>
                                    New members can Join and pay for their membership through easy to follow forms.
                                    Renewals can be easily configured to meet your Associations requirements (Cyclical, Rolling or Anniversary), and be configured to be partially or Fully Automated.
                                    When a member Joins, it automatically creates a profile and moves them into workflows that makes processing and approving that new member a trivial task.
                                    Renewals can partially or fully Automated to an extent that renewals require no manual tasks at all.
                                    Joining and Renewing from the members perspective is very simple, contributing to increased membership growth and retention.
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-sliders-h fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Highly configurable Membership Types and related processes</h3>
                                <p>
                                    Easily create and configure Membership Types.  Individual Membership Types can be configured individually taylor both your internal work flows as well as how each Membership Type Interacts with your systems  workflows.
                                    Configure what fields they can view and edit, how Joins and Renewals are processed, what pricing options they have available and more.
                                    Each membership type is different, and being able to configure each type to best suit their needs is a critical aspect in providing value and increasing engagement of your members, while ensuring your workload around these objectives is kept to a minimum.
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-bolt fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Enable Members with Self Servicing to increase Engagement while Reducing Admin Workload</h3>
                                <p>
                                    Empower your members and stakeholders by giving them more control (while reducing your workload).
                                    You choose what data they have access to and update against their profile as well as how they engage with other areas of the system from public or private directories, Special Interest Groups and what communications they receive through to your members easily accessing their own invoices, histories, related members and more through an intuitive and easily accessed Member Dashboard.
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="far fa-id-card fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Members can manage their own profiles, view historical invoices, events and notes</h3>
                                <p>
                                    Your members information needs to be kept up to date if you are going to appropriately service their needs, effectively communicate with them and know where you stand with them.
                                    Your members also need to be able to access all of their information when needed, and in a way that is convenient for them (and your staff).
                                    membes provides all necessary tools to allow your members to update their information, view invoices and details of past events attended, certificates and more.
                                    Importantly, it also gives you full control of what they can or can't see and update themselves, as well as all updates being recorded so you can easily see who changed something, when it was changed and what it was changed from and to.
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-building fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Effectively cater to and manage Organisation and Individual Members</h3>
                                <p>
                                    membes provides the ability to manage and cater to both Organisational and Individual members side by side.
                                    Ensure each stakeholder type is being managed most appropriately and in a way each member feels most valued.
                                    Setup Organisations and staff with a highly configurable and powerful relationship engine that gives greater control to you while signficantly adding value to your Organisations Members.
                                    Organisations can view or even manage their staffs profile, event attendance, invoice, CPD history and more.
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-code-branch fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Vital relationships between Profiles can be created and managed with ease</h3>
                                <p>
                                    membes has a powerful Relationship Engine which allows you to setup any type of Relationship between profiles that exists with your members.
                                    Employees and Staff, Trainers and Trainees, Family members, whatever your organisations relationship requirements are membes can be configured to cater to it.
                                    Profiles can also be configured to see as well as manage related profiles. The benefits to you of allowing Employees to view and manage their Employers profiles are significant, not to mention the value this brings to your members.
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-chart-pie fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Granular and real time Segmentation</h3>
                                <p>
                                    Because membes is a holistic, integrated, single point of truth system.  The System is capturing and storing all information, activities and histories of all your members in one central location.
                                    This allows very targeted and insightful segmentation of your Stakeholders and a level of depth and accuracy not available in other 'generic' or 'fragmented' systems.
                                    Generate Reports, exports and even Heat Map visualisations of your data in a way that gives you knowledge, insights and awareness of your membership and stakeholder database that are critical in ensuring your communications are well targeted and you know where your membership is at and where it is going.
                                </p>
                            </td>
                        </tr>

                    </tbody>
                </table>

            </div>

         </div>
    </div>
</div>


<div class="tempcore-line">
    <div class="container">
        <a href="/demo/" style="width: 220px">Request FREE Demo</a>
        <p><i class="fa fa-thumbs-up"></i>Future Proof your Association with current best practice in technologies and standards.</p>
    </div>
</div>


</cfoutput>   