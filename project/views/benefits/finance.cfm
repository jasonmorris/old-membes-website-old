<cfoutput>

<div class="page-banner page-banner-narrow page-banner-crowd-blue">
	<h2>Benefits for finance and real time payments</h2>
</div>

<div class="page-intro">
    <div class="title-section">
        <div class="container">

             #renderView('_includes/benefits_nav')#

        	<div class="text-holder">
                 <h1>Finance and Payments</h1>
                <p><b>Although membes does not replace your financial package, it provides considerable efficiencies through automation of invoice generation, synchronisation with finance packages and reporting.</b></p>

                <p>
                    It is important that your members and stakeholders are able to pay for your products and services easily and via a method and medium that is convenient.
                    Being able to take payments direct to your Bank Account, with minimal Transaction Fees, integrated Invoice Generation, Reporting and Accounting Software integration is a fundamental requirement of a Professional Association.
                </p>

                <table border="0" width="100%" class="infotable">

                    <thead>
                        <tr>
                            <th colspan="3">Finance</th>
                        </tr>
                    </thead>

				    <tbody>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-piggy-bank fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Receive payments direct to your bank account through all major Australian Payment Gateways.</h3>
                                <p>
                                    It is critical for a Professional Association to receive money direct to your bank account and with minimal transaction fees.
                                    As your Association takes more and more payments online, having the right Credit Card payment methods is vital in ensuring that large percentages of that revenue doesn't get lost in trasaction fees.
                                    To make it easy for you to increase your level of online payments without blowing out transaction costs, membes integrates with all the major Australian Payment Gateways.
                                    <a target="_blank" href="https://www.securepay.com.au/">SecurePay</a>,
                                    <a target="_blank" href="https://www.eway.com.au/"> eWay</a>,
                                    <a target="_blank" href="https://www.paypal.com/au/webapps/mpp/payflow-payment-gateway">Payflow Pro</a>,
                                    <a target="_blank" href="https://www.westpac.com.au/business-banking/merchant-services/online-payments/payway-api/"> PayWay</a> and
                                    <a target="_blank" href="https://www.braintreepayments.com/en-au"> Braintree</a>.
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-file-alt fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Invoices for payments received / pending are generated within membes allowing management by Administrators and self access by Members.</h3>
                                <p>
                                    Invoices are automatically generated whenever a payment is made, or payment is required.
                                    These invoices are generated and stored within the membes platform, and as such are a part of the Single Point or Truth system, providing significant advantages in terms of:

                                    <br>
                                    - Reporting on financial data against other data generated by the system<br>
                                    - Invoices for your members and stakeholders are stored in one place along with all other information about those stakeholders<br>
                                    - Members can search, view, download and print past invoices from their members portal<br>

                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <img src="/includes/img/logo/xero.png" width="70px">
                            </td>
                            <td>
                                <h3>Tight integration with XERO ensures complete and real time synergy between your AMS and Accounting Software</h3>
                                <p>
                                    membes has integrates very tightly with the XERO Accounting Software. This integration is <b>much</b> more than just an export and import.
                                    You can synchronise membes with the Account Mapping and Tracking codes that you have configured in your XERO account.
                                    Once this synchronisation is done, you can than map your XERO mapping and tracking codes with all eCommerce items you sell within membes.  Shop Purchases, Event Registrations, Joinin fees, Renewal fees and more.
                                    Because the synchronisation is real time, financial data within XERO is always up to date (to the second) without need for erroneous exports and imports.
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-download fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Export Invoices, Invoice items and Online payments for advanced reporting an analytics or importing into your preferred Accounting Software</h3>
                                <p>
                                    If you don't use XERO, you can export all necessary transaction data for advanced custom reporting and importing into other software.
                                    Invoices and Invoice items can be listed or exported for importing into other software.
                                    Online Payments can be exported for easy reconciling with your Payment Gateway and Bank.
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-chart-line fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Export financial data for custom reporting, or membes includes insightful Dashboard reports around your organisations financial activities.</h3>
                                <p>
                                    Dashboard reports provide vital insights into the financial position and makeup of your organisation.
                                    View percentage breakdowns of revenue by Membership Type, Location and Revenue Sources.
                                    See these breakdowns over time to see what is happening through 3, 6 and 12 monthly snapshots.
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-cogs fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Automated generation and sending of Renewal invoices and Online Renewal form for full self servicing by members.</h3>
                                <p>
                                    Renewal notices and invoices can be configured to be sent out on a members Anniversary or have sent to everyone at the same time of the year.
                                    Configuration can be done in many ways from fully manually, all the way through to set and forget where renewal invoices are sent via email as needed and when members receive and process their renewal all statuses are updated with nothing more to do until the next renewal invoice is automatically generated and sent
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td class="benefit-feature-icon" width="10%">
                                <i class="fas fa-dollar-sign fa-5x green"></i>
                            </td>
                            <td>
                                <h3>Fully Automated eCommerce, Event Registrations and Members Fees creates opportunities to scale multiple revenue streams.</h3>
                                <p>
                                    Healthy and profitable revenue streams are vital for the long term health and growth of your organisation.
                                    The membes platform provides a range of revenue stream options that can be configured for full automation, meaning your can increase total revenue while not having an association increase in workload.
                                    Not only can all revenue streams be automated and scale, but they are linked direct to your bank and processed with a minimal transaction fee ensuring there is no leakage of your revenue growth.
                                </p>
                            </td>
                        </tr>

                    </tbody>
                </table>

            </div>

         </div>
    </div>
</div>


<div class="tempcore-line">
    <div class="container">
        <a href="/demo/" style="width: 220px">Request FREE Demo</a>
        <p><i class="fa fa-thumbs-up"></i>Future Proof your Association with current best practice in technologies and standards.</p>
    </div>
</div>


</cfoutput>   