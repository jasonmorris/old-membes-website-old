<cfoutput>

<div class="page-banner page-banner-landpage1">

	<h1 style="margin-top:220px;">Membership Software for Associations and NFPs</h1>
    <h2>Membes is a holistic software that provides Membership Management, CRM, Online Payments, Invoicing, Communications and more.</h2>
    <div>

        <div style="margin:20px auto 0 auto; padding: 5px; max-width:500px;">

            <form name="request_more_info" method="post" action="#event.buildLink('demo/')#">

                <div style="padding:5px; display:inline-block;float:left;text-align:left;">
                    <!--<input type="text" name="website" class="inputtext" placeholder="Enter your website" style="width:300px;" />-->
                </div>

                <div style="padding:5px; display:inline-block; float:left;">
                    <input type="submit" class="btn btn-success" value="Try membes">
                </div>

            </form>

        </div>
    </div>

</div>


<div class="page-intro">
    <div class="title-section">
        <div class="container" style="text-align:center;">      
            <h1>Discover membes Association Software</h1>
            <br />
            <div class="no-underline">
                <a href="/about/explainer-video/"><div class="play-video">&##xf144;</div></a>
                <br />
                <p>Understand membes with our 90 second video</p>
            </div>
            
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            
            <h1>Association Management Software without the price tag</h1>
            <br />
            
            <div class="landing-feature-shell">
                <div class="landing-feature">              
                	<div class="thing1">&##xf155;</div>
                    <strong>Reduce Costs</strong>
                    
                    <p>Lower initial and on-going costs with rapid deployment. Don't pay extra for extra functionality.</p>
                </div>
                <div class="landing-feature">
                	<div class="thing1">&##xf013;</div>
                    <strong>Freedom & Flexibility</strong>
                    <br />
                    <p>Reduce reliance on third party providers, where you have more control to manage your content.</p>
                </div>
                <div class="landing-feature">
                	<div class="thing1">&##xf080;</div>
                    <strong>Streamline Workflows</strong>
                    <br />
                    <p>Automated workflows eliminate many of the cumbersome member & event administrative tasks.</p>
                </div>
            </div>
            
        
            
            
            <p>&nbsp;</p>
        </div>
    </div>
</div>   

<div style="background:##eee;">
	<div class="title-section" style="margin-bottom:0;">
        <div class="container" style="padding:25px; text-align:center;">
        	<h1>Cloud SaaS Software</h1>
            <br />
            <p>membes is a full stack Software as a Service (SaaS) Solution designed specifically for Australian Associations. SaaS has fast become the preferred software delivery model as it provides high end capabilities for a fraction of the cost. Low Setup, Low ongoing costs, no need for software upgrades or compatibility maintenance. For a fraction of traditional costs, your Associations Website, CRM and Online Capabilities remain future proofed, without the need of costly upgrades or rebuilds.</p>
            <p>&nbsp;</p>

            
            
            <a href="#event.buildLink('about.membership-software/')#" class="btn btn-info btn-lg btn-banner">Detailed Feature List</a>
        </div>
    </div>
</div>

<div class="page-intro">
    <div class="title-section">
        <div class="container" style="text-align:center;">
            <h1>Integrated Website & Membership Database</h1>



            <br />
            <h2 style="font-size:18px;">membes single database solution</h2>
            <p>Many organisations still pursue the avenue of a separate CRM and website. This disconnect between systems creates a disconnect or even loss of potential data. membes helps you leverage your data using a single point of truth database with interactive front-facing portals for your members and stakeholders (i.e. your website) to an intuitive and easy to use management and analysis portal (i.e. your administrative  'back-end'). This increased connectivity leads to improved reporting, insights, and more targeted communications based on data collected. Ensuring your association continues to remain relevant to its users and stakeholders.
</p>
			<br />
            <img alt="Integrated membes cloud-based Software as a Service (SaaS) Membership and Association Management" src="/includes/img/content/saas-integration-solution.jpg" class="img-responsive" style="display:inline; max-width:427px;">
            <p>&nbsp;</p>
            <p>&nbsp;</p>

            <h2 style="font-size:18px;">separate website, events & membership database software</h2>

            <p>The choice of a separate CRM and separate website might appear initially attractive and cost-effective, but ultimately is a missed opportunity and usual leads to higher costs over time. Because of the increased complexity involved, disparate or silo-ed systems will often stay stagnant or degrade over time leading to decreased security, reliability, upgradeability, productivity and members disengagement.</p>
            <br />
            <img alt="Non-integrated member CRM, events, communications, education and website (CMS) software" src="/includes/img/content/non-integrated-member-software.jpg" class="img-responsive" style="display:inline; max-width:466px;">
            <p>&nbsp;</p>
        </div>
    </div>
</div>


<div class="tempcore-line tempcore-line-noshadow" style="background-color: ##3e4347">
    <div class="container">


		<p>
			<q style="font-size:24px;">Membes has proved to be both cost-effective and feature rich for a number of our clients who are utilising the platform. From contact management to emarketing, online shops to member locators, CPD tracking to complex conference management, membes seems to have it all. It caters to both administrators and users. We wouldn't hesitate to recommend the membes platform.</q>
			<br /><strong>Elaine Trevaskis</strong>
            <br />Association Professionals
		</p>
    </div>
</div>


<div class="home-panel" style="padding: 74px 0 33px;">
    <div class="container">
        <div class="row">

			#renderView('_includes/clients')#

        </div>
    </div>
</div>


<div class="tempcore-line">
    <div class="container">
        <a href="/demo/" style="width: 220px">Try Membes</a>
        <p><i class="fa fa-thumbs-up"></i>Future Proof your Association with current best practice in technologies and standards.</p>
    </div>
</div>

</cfoutput>    