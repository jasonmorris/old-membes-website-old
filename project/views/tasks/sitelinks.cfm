
<style>
    p {margin-bottom: 10px;}
</style>

<cfoutput>

    <div class="well">

    <form name="servers" action="" method="post">

    <select name="server" onchange="this.form.submit()" class="form-control">

        <option value=""> -- SELECT SERVER -- </option>
        <cfloop query="rc.servers">
            <option value="#rc.servers.membes_server#" <cfif rc.server == rc.servers.membes_server> selected</cfif> >#rc.servers.membes_server#</option>
        </cfloop>
    </select>

    </form>


    <cfif structKeyExists(rc,'sites')>

        <p><br></p>

        <cfloop query="rc.sites">
            <p>
                <a href="http://#rc.sites.domain#/" target="_blank" class="btn btn-info" style="width:75%">#rc.sites.name# [#rc.sites.name_abbr#]</a>
                <a href="http://#rc.sites.domain#/?fwreinit=1" target="_blank" class="btn btn-success pull-right" style="width:20%">[#rc.sites.name_abbr#] init</a>
            </p>
        </cfloop>

        <p>
            <a href="http://admin.membes.com.au/" target="_blank" class="btn btn-info" style="width:75%">*ADMIN</a>
            <a href="http://admin.membes.com.au/?fwreinit=1" target="_blank" class="btn btn-success pull-right" style="width:20%">*ADMIN init</a>
        </p>

    </cfif>


    </div>

</cfoutput>