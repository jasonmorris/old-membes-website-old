<cfoutput>

<div class="page-banner page-banner-puzzle">
	<h2>A platform to build on...</h2>
</div>

<div class="page-intro">
    <div class="title-section">
        <div class="container">
			<div class="text-holder"> 
            
            <h1>A Platform for Developers</h1>     
            <p>Membes platform is more than just a service but a platform you can build on. Design the look and feel through HTML, CSS and JS but build on solid Stakeholder and Association Management Software (AMS). Developers and designs can maintain their existing relationships with clients or pitch to new clients without having to pass on the huge expense of developing from the ground up.</p>
            <br>
            
            <ul class="modernlist ticklist">
            
            <li><strong>Designers can maintain existing relationships</strong>
            <br>Style and design beautiful looking websites through traditional HTML5, CSS and JS but build on top of a class-leading platform that still delivers the functionality clients need.<br><br></li>	
            
            <li><strong>Existing website? No problem!</strong>
            <br>Many organisations use the complete package of membes to seamless replace their existing website, membership and events software, but you can also use membes as a membership portal to site alongside any existing website(s). You can also use membes API to pull across news, events or even for authentication so membes remains your “single point of truth” database.<br><br></li>
            
            <li><strong>Membes API</strong>
            <br>Need to connect multiple sites, web applications, Learning Management Systems (LMS) or mobile applications to a central “single point of truth” database? Yes, membes is that platform. Membes comes with many out of the box integrations but it also comes with its own extensive API and web-hooks designed to connect to many 3rd party systems.<br>
            
            <br>Consumer sites can pull event or news information from membes, the online store can be used with web-hooks to push information to online course software or Learning Management System (LMS), as well as insert CPD activity information back to membes on completion of courses.<br>
            
            <br>Designed a mobile application but face the challenge of synchronising profile or login databases? Just use membes’ authentication API to validate users.</li>
            
            </ul>
  
            <p>&nbsp;</p>	
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            </div>
        </div> 
    </div>
</div>


</cfoutput>   