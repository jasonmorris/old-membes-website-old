<cfoutput>
<!-- slider 
    ================================================== -->
<div id="slider" class="revolution-slider">
    <!--
    ##################################################################
        - THEMEPUNCH BANNER -
    ##################################################################
    -->

    <div class="fullwidthbanner-container" style="box-shadow:0 0 25px rgba(0,0,0,0.2);">
        <div class="fullwidthbanner">
            <ul>
                <!-- THE FIRST SLIDE -->
                <li data-transition="fade" data-slotamount="0" data-masterspeed="300" data-delay="7000">
                    <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                    <!-- <img alt="" src="/includes/img/banner/crowd_heads.jpg" > -->
                    <img alt="" src="/includes/img/banner/crowd_heads.jpg">
                    
                    
                    <cfset dataIconEnd = 0>
                    <cfset dataEnd = dataIconEnd>
                
                    <!-- THE CAPTIONS IN THIS SLDIE -->
                    <div class="caption large_text lft"
                         data-x="530"
                         data-y="70"
                         data-speed="1200"
                         data-start="1200"
                         data-easing="easeOutExpo" data-end="#dataIconEnd#" data-endspeed="300" data-endeasing="easeInSine" id="slide-heading">
                            <span id="slider-logo-btn">m</span>
                         </div>
                    
                    <div class="caption large_text lft"
                         data-x="40"
                         data-y="220"
                         data-speed="1200"
                         data-start="1400"
                         data-easing="easeOutExpo" data-end="#dataIconEnd#" data-endspeed="300" data-endeasing="easeInSine" id="slide-subheading">
                            Stakeholder Management for Australian Associations<br>
                            <div style="text-align: center; font-size:28px; margin-top: 18px">For your membership, events, website management and much more ...</div>
                            </div>

                    
                    <cfset iconRow1PosStart = 330>
                    <cfset iconRow1PosAdd = 350>
                    <cfset iconRow1TimeStart = 1800>
                    <cfset iconRow1TimeAdd = 50>							
                    <cfset thisIcon = 0>
                    
                    <div class="caption lft"
                         data-x="#iconRow1PosStart#"
                         data-y="360"
                         data-speed="600"
                         data-start="#iconRow1TimeStart#"
                         data-easing="easeOutExpo" data-end="#dataIconEnd#" data-endspeed="300" data-endeasing="easeInSine" id="slide-btn-findoutmore">
                            <a href="#event.buildLink('about.membership-software/')#" class="btn btn-info btn-lg btn-banner">List of Features</a>
                        </div>
                        
                    <cfset thisIcon = thisIcon + 1>
                    <cfset posX = iconRow1PosStart + (iconRow1PosAdd * thisIcon)>
                    <cfset timeStart = iconRow1TimeStart + (iconRow1TimeAdd * thisIcon)>
                    <div class="caption lft"
                         data-x="#posX#"
                         data-y="360"
                         data-speed="600"
                         data-start="#timeStart#"
                         data-easing="easeOutExpo" data-end="#dataIconEnd#" data-endspeed="300" data-endeasing="easeInSine"  id="slide-btn-getstartednow">
                            <a href="#event.buildLink('demo/')#" class="btn btn-success btn-lg btn-banner">Request FREE Demo</a>
                        </div>
                        
                </li>
               
                
            </ul>
        </div>
    </div>
	
	<div id="resp-banner-home" class="page-banner page-banner-home-resp visible-xs">
		<h2>Stakeholder Management<br><small>for</small><br>Australian Associations</h2>
		<a href="#event.buildLink('about.features')#" class="btn btn-transparent btn-lg btn-banner">Find out more</a>
		<a href="#event.buildLink('contact')#" class="btn btn-success btn-lg btn-banner">Get Started Now</a>
	</div>
	
	
	
	
</div>
<!-- End slider -->

<a id="features"></a>

</cfoutput>