﻿
<cfoutput>


#renderView('home/slider')#
<!---

<div class="page-banner page-banner-home" id="slide-heading">

	<h1> Association Software for Australian Associations</h1>

	<div style="margin:auto; width: 800px;text-shadow: 0 0 10px ##000;">
		<div style="width: 400px; float:left">
			<ul class="fa-ul">
				<li><i class="fa-li fa fa-check-square"></i>Integrated, Modern, Easy to Manage Website</li>
				<li><i class="fa-li fa fa-check-square"></i>Members Only features and pages</li>
				<li><i class="fa-li fa fa-check-square"></i>Mobile and Touch Device Compatible</li>
				<li><i class="fa-li fa fa-check-square"></i>Private Groups Portals</li>
			</ul>
		</div>
		<div style="width: 400px; float:right">
			<ul class="fa-ul">
				<li><i class="fa-li fa fa-check-square"></i>Full Featured Membership Database</li>
				<li><i class="fa-li fa fa-check-square"></i>Online Joining, Renewals and Payments</li>
				<li><i class="fa-li fa fa-check-square"></i>Events Management</li>
				<li><i class="fa-li fa fa-check-square"></i>CPD Tracking & Auditing</li>
			</ul>
		</div>
	</div>

	<div id="home-banner-btns">
		<a href="#event.buildLink('about.membership-software/')#" class="btn btn-info btn-lg btn-banner">Detailed Feature List</a>
		<a href="#event.buildLink('demo/')#" class="btn btn-success btn-lg btn-banner">Request FREE Demo</a>
	</div>



	
</div>

--->
<div class="right-img home-panel services-box3" style="padding-bottom: 10px; padding-top: 30px;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="text-holder" style="height:168px; display:table;">
					<span style="vertical-align:middle; text-align:center; display: table-cell;">
					<h2>Why membes Association Software?</h2>
					<p>Watch our 90 second explainer video to get a quick overview of what membes can deliver to you, your Association and your Members. </p>
					</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="img-holder" style="text-align: center">

					<cfif structKeyExists(session,'sawHomeVideo')>

						<a href="/about/explainer-video/">
							<img alt="" src="/includes/img/content/video-placeholder-1.png" class="img-responsive" style="display:inline; box-shadow: 0 0 25px rgba(0,0,0,0.2);">
						</a>

					<cfelse>
						<div style="max-width:420px; margin:0 auto;">
						<iframe src="https://player.vimeo.com/video/169498020?autoplay=1&loop=1" width="100%"  frameborder="0" style="height:240px;"></iframe>
                        </div>
						<br><a href="/about/explainer-video/" style="font-size: 12px">view in larger format >>> </a>
						<cfset session.sawHomeVideo = true>

					</cfif>

                </div>
            </div>

        </div>
    </div>
</div>


<div class="left-img home-panel" style="padding-top: 25px; padding-bottom:25px;">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="img-holder">
					<img alt="Association Management - Members, Communications, Website, Education" src="/includes/img/content/ams-pie2.png" class="img-responsive" style="max-width:350px; margin: 35px auto; width:100%;">
				</div>
			</div>
			<div class="col-md-6">
				<div class="text-holder" style="min-height:350px; display:table;">
					<span style="vertical-align:middle; text-align:center; display: table-cell;">
					<h2>Whatever your<br>association needs,</h2>
					<h3>membes has it sown right in.</h3>
					<p>membes is cloud bases Association Software available as SaaS (Software as a Service) and designed to remove the hassle and costs of system and module integration with all the different facets of an associations needs sown right in.  Website, CRM, Event Management, CPD to name just a few.</p>
					<p>Or, if you have a need that membes doesn't include, then we are happy to discuss adding it.</p>
					
					</span>
				</div>
			</div>
			
		</div>
	</div>
</div>

<div class="services-box3">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="text-holder" style="height:300px; display:table;">
					<span style="vertical-align:middle; text-align:center; display: table-cell;">
					<h2>Anytime, Anywhere, on Any Device</h2>
					<p>Cloud based hosting and fully responsive UI's ensure your Association Website, Administration Console, Groups Portals, Education and Collaboration and more can all be accessed buy all stakeholders from whatever device or location they prefer.  This is no longer a luxury, but a necessity in maximising stakeholder engagement.</p>
					</span>
				</div>
			</div>
			<div class="col-md-4">
				<div class="img-holder">
					<img alt="" src="/includes/img/content/devices.png" class="img-responsive" style="margin: 0 auto;max-height:300px; width:100%;">
                </div>
			</div>
			
		</div>
	</div>
</div>


<div class="left-img home-panel">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="img-holder">
					<img alt="" src="/includes/img/content/cloud-enterprise.png" class="img-responsive" style="max-width:350px; margin: 0 auto; margin-bottom:20px; width:100%;">
				</div>
			</div>
			<div class="col-md-9">
				<div class="text-holder" style="height:168px; display:table;">
					<span style="vertical-align:middle; text-align:center; display: table-cell;">
					<h2>SaaS | Enterprise Association Management Software without the Enterprise Price Tag</h2>
					<p>membes is a full stack Software as a Service (SaaS) Solution designed specifically for Australian Associations.  SaaS has fast become the preferred software delivery model as it provides high end capabilities for a fraction of the cost.  Low Setup, Low ongoing costs, no need for software upgrades or compatibility maintenance.
					For a fraction of traditional costs, your Associations Website, CRM and Online Capabilities remain future proofed, without the need of costly upgrades or rebuilds.</p>
					<p>Take Office 365 as an example.  It used to cost thousands to purchase and upgrade, but now that is available as a SaaS solution, and costs a fraction of what it used to.
					<a href="http://en.wikipedia.org/wiki/Software_as_a_service" target="_blank">Read more about SaaS here.</a></p>
					</span>
				</div>
			</div>
			
		</div>
	</div>
</div>

<div class="tempcore-line tempcore-line-noshadow" style="background-color: ##3e4347">
    <div class="container">

		<h2 style="color:white">What do our users say?</h2>

		<p>
			<q>The office staff love the admin site - and can quickly respond to member enquiries regarding membership, event registration, CPD tracking and can easily send communications to a selected subset of the database</q>
			<br><span class="greysmall">Melinda Caspersz, GENCA.</span>
		</p>
		<hr style="width: 25%;" align="left">
		<p>
			<q>Membes has proved to be both cost-effective and feature rich for a number of our clients who are utilising the platform. From contact management to emarketing, online shops to member locators, CPD tracking to complex conference management management, membes seems to have it all. It caters to both administrators and users.  We wouldn't hesitate to recommend the membes platform.</q>
			<br><span class="greysmall">Elaine Trevaskis, Association Professionals</span>
		</p>
		<hr style="width: 25%;" align="left">
		<p>
			<q>A great system that is very easy to use from an operational stand point. We can keep track of communication of our members, issue certificates at the touch of a button. Run our own events from the one site. Simple, easy and great customer service from the team at membes.</q>
			<br><span class="greysmall">Jess Ryce, RSA.</span>
		</p>

    </div>
</div>


<div class="home-panel" style="padding: 33px 0 33px;">
    <div class="container">
        <div class="row">

			#renderView('_includes/clients')#

        </div>
    </div>
</div>



<div class="tempcore-line">
    <div class="container">
        <a href="/demo/" style="width: 220px">Request FREE Demo</a>
        <p><i class="fa fa-thumbs-up"></i>Future Proof your Association with current best practice in technologies and standards.</p>
    </div>
</div>

</cfoutput>
