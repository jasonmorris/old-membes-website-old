
<cfoutput>

	<div class="page-banner page-banner-whousing">
		<h2>Who is currently using the membes platform to run their Association?</h2>
	</div>

	<cfquery name="clients">
        SELECT * FROM org WHERE membes_reference IS NOT NULL ORDER BY site_dsp_order
    </cfquery>

<div class="statistic-section">


        <div class="title-section">
            <div class="container">
                <h1>Who is using membes</h1>
                <p>The following Associations are using membes to manage their Stakeholder Database, Website, Membership Joining and Renewals, Events Management, Internal Communications and much more..</p>
            </div>
        </div>



	<div>
        <div class="container" style="padding: 30px;">

			<div class="row">

			<cfloop query="#clients#">
				<div class="col-md-4" style="padding: 20px; height: 250px">

					<div style="#membes_reference#">

						<a href="http://#domain#" target="_blank">
							<img src="http://#domain#/public/#id#/website/logo-head.png" style="max-width: 270px; margin: auto">
						</a>

					</div>

				</div>

			</cfloop>

			</div>

		</div>
	</div>

	<div>
        <div class="container">
			<div class="row">
				<div class="col-md-6">
					<a href="/about/explainer-video" class="btn btn-info otherbtn">
						90 Second Overview
						<i class="fa fa-angle-double-right" aria-hidden="true"></i><i class="fa fa-angle-double-right" aria-hidden="true"></i><i class="fa fa-angle-double-right" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-6">
					<a href="/demo/" class="btn btn-success otherbtn">
						Request your FREE Demo
						<i class="fa fa-angle-double-right" aria-hidden="true"></i><i class="fa fa-angle-double-right" aria-hidden="true"></i><i class="fa fa-angle-double-right" aria-hidden="true"></i>
					</a>
				</div>
			</div>
		</div>
	</div>

				<br><br>

</div>

<div class="tempcore-line">
    <div class="container">
        <a href="/demo/" style="width: 220px">Request FREE Demo</a>
        <p><i class="fa fa-thumbs-up"></i>Future Proof your Association with current best practice in technologies and standards.</p>
    </div>
</div>



</cfoutput>