
<div class="page-banner page-banner-technical">
	<h2>well ... it's technical.  read on.</h2>
</div>


<div class="pricing-box">

	<div class="title-section">
        <div class="container">
            <h1>Implementation Options</h1>
            <p>Other Association Management Software and CRM's either come as a SaaS, Stand Alone Hosted or Self Hosted solution.  <b>membes</b> is available as all three; SaaS, Stand Alone Hosted solution or Self Hosted platform.  More importantly, you can transition through these implementations options over time as your organisations needs and demands grow.  You can start out using <b>membes</b> as a low cost SAAS solution, then, if and when your Association becomes larger with greater and more complex needs, we can easily transition your Association across to a Hosted or Self Hosted Solution.</p>
        </div>
    </div>

    <div class="container">
        <div class="row">

			<div class="col-md-4">
                <ul class="pricing-table basic">
                    <li class="title">
                        <p>Licensed SaaS</p>
                        <span>
                            Licensed Software as a Service
                            <br><b>recommended</b>
                        </span>
                    </li>
                    <li class="li-left"><p> <i class="fa fa-plus-square"></i> Low cost setup</p></li>
                    <li class="li-left"><p> <i class="fa fa-plus-square"></i> Very competitive licensing options</p></li>
					<li class="li-left"><p> <i class="fa fa-plus-square"></i> Up and Running in 1-3 weeks</p></li>
                    <li class="li-left"><p> <i class="fa fa-plus-square"></i> Pay nothing for new features and upgrades as they are released.</p></li>
                    <li class="li-left"><p> <i class="fa fa-plus-square"></i> Very customisable.  Covers all needs of 95% of Associations.</p></li>
                    <li class="li-left"><p> <i class="fa fa-plus-square"></i> You have a big say on what features are developed and added, via our New Feature Request Portal.</p></li>
                    <li class="li-left"><p> <i class="fa fa-plus-square"></i> Includes everything.  No expensive add-on modules.</p></li>
                    <li class="li-left"><p> <i class="fa fa-plus-square"></i> Hosting and Security are Covered</p></li>
                    <li class="li-left"><p> <i class="fa fa-plus-square"></i> 24/7/365 Monitoring and Full Daily Backups.</p></li>

                </ul>

            </div>


            <div class="col-md-4">
                <ul class="pricing-table basic heading-blue">
                    <li class="title">
                        <p>Stand Alone - Hosted</p>
                        <span>Your own, fully customisable instance.<br>Hosted and Maintained by us for you.</span>
                    </li>
                    <li class="li-left"><p> <i class="fa fa-plus-square"></i> Provides a completely standalone installation of members.</p></li>
                    <li class="li-left"><p> <i class="fa fa-plus-square"></i> Most customisation requirements are built in.</p></li>
					<li class="li-left"><p> <i class="fa fa-plus-square"></i> Any requirement on customisation beyond built in customisations can be developed.  There are no limitations</p></li>
					<li class="li-left"><p> <i class="fa fa-plus-square"></i> Hosting and Security are Covered</p></li>
					<li class="li-left"><p> <i class="fa fa-plus-square"></i> 24/7/365 Monitoring.  Daily Backups.</p></li>
                    <li class="li-left"><p> <i class="fa fa-plus-square"></i> Move to "Stand Alone - Hosted" from "Licensed SaaS" at any time.</p></li>
                    
                </ul>
            </div>

             <div class="col-md-4">
                <ul class="pricing-table basic heading-blue">
                    <li class="title">
                        <p>Stand Alone - Self Hosted</p>
                        <span>
                            Your own, fully customisable instance. <br>Hosted on your own servers.
                        </span>
                    </li>
                    <li class="li-left"><p> <i class="fa fa-plus-square"></i> Provides a completely standalone installation of members.</p></li>
                    <li class="li-left"><p> <i class="fa fa-plus-square"></i> Hosted entirely on your infrastructure.</p></li>
                    <li class="li-left"><p> <i class="fa fa-plus-square"></i> Most customisation requirements are built in.</p></li>
                    <li class="li-left"><p> <i class="fa fa-plus-square"></i> Any requirement on customisation beyond built in customisations can be developed.  There are no limitations, whether it be customised expansions on existing modules, or development of an entirely new module.</p></li>
                    <li class="li-left"><p> <i class="fa fa-plus-square"></i> Can run on a Windows or Linux Server</p></li>
                    <li class="li-left"><p> <i class="fa fa-plus-square"></i> Only requirement is Java and mySQL.  Both Open Source and Free.</p></li>
                    <li class="li-left"><p> <i class="fa fa-plus-square"></i> Move to "Stand Alone - Hosted" from "Licensed SaaS" at any time.</p></li>
                    
                </ul>
            </div>

        </div>
    </div>

</div>



<div>
    <div class="title-section">
        <div class="container">
            <h1>Enterprise Architecture</h1>
            <p><b>membes</b> is a cloud hosted SAAS Association Management Software, developed inline with the current trends in architecural best practice.  Your Website and Administration become accessible anywhere and optimised for any device.  <b>membes</b> runs on the latest enterprise technologies, architecture and hardware.  This ensures speed, security, stability, scalability and low cost maintenance & feature enhancements.</p>
            <br>
            <p>
	            <ul class="modernlist ticklist">
					<li>Anywhere, Any Time, on Any Device!  Both Website and Administration have been optimised for Mobile, Tablet and Desktop.</li>
                    <li>High availability cloud hosting.  Ensuring maximum uptime, speed and security.</li>
                    <li>24x7x365 monitoring of server availability and performance.</li>
                    <li>Daily system wide backups ensure there is never a chance of lost data or information.</li>
                    <li>All running on Open Source Linux, Apache and mySQL for maximum performance and minimum cost.</li>
				</ul>		
			</p>
		<p>&nbsp;</p>
        <p>&nbsp;</p>
        </div>
    </div>
</div>


