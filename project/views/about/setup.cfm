
<cfoutput>

<div class="page-banner page-banner-setup">
	<h2>up and running in <span>weeks</span>.  easy as 1,2,3..4</h2>
</div>

<div class="page-intro">
    <div class="title-section">
        <div class="container">
            <h1><i class="fa fa-connectdevelop"></i> Getting Setup</h1>
            <br>
            <p>
            	Traditionally, setting up Enterprise Grade Assocation Management Software takes months, with lots of money committed before anything tangible is produced.  However with <b>membes</b>, we can have your Website, Membership Database and Administration Interface all up and running in 2-4 weeks.
            <p>
            <br>
            <p>
            	Although we can get your Website and Membership Database setup very quickly, setting up a system like this requires proper consideration and informed decisions.  We help ensure you make the right decision about <b>membes</b> with the following process:
            </p>
            <br>
            <hr>
	            <ul class="fa-ul feature-overview-list circle-number-list">
					<li>
						<div class="circle-number">1</div>
						<div>
	                        <h3>Request a Demo</h3>
	                        <p>The first step is to <a href="#event.buildLink('demo/')#">Request a Demonstration</a>.  Shortly after we have received your request, we will contact you to discuss your needs in more detail and make sure <b>membes</b> is a suitable solution for your Organisation.  About one week after your Demo Request, the trial version of membes will be made available for yourself and other key stakeholders in your organisation to play with and discuss.  This is a private (mostly functional, but not fully) Trial version of the membes platform setup specifically for you, so it gives you a real chance to get a good feel for it's capabilities and fit for your Organisation.
	                    	</p>
                        </div>
				  	</li>

				  	<li>
						<div class="circle-number">2</div>
						<div>
	                        <h3>Demo Walk Through and Q&A Session</h3>
	                        <p>
	                        	After you have had a few days to play with your trial, at a time that suits you, we will arrange a 1-2 hour phone hookup.  This will be a conference call that any of your decision makers can join in on.  This will give us a change go through the key features with you, and give you a chance to ask any questions you may have.  If at any time during the Trial you have any other questions, we are always available to discuss.
	                    	</p>
                        </div>
				  	</li>
                    
                    <li>
						<div class="circle-number">3</div>
						<div>
	                        <h3>Setup</h3>
	                        <p>
	                        	Assuming you were happy with what you saw, we then move to start setting up your Website and Membership Database.  We do this together with a nominated person in your organisation, as some of the setup steps can be done by you via the Administration Interface:
	                        	<ol class="list">
	                        		<li><p>We setup membes for you and make it available online for you to setup your customisations (e.g. Custom Fields, automated emails, website content etc.)</p></li>
	                        		<li><p>You provide us with your organisations style guide, which we use to design your website.  If you don't have a style guide we can assist you with this.</p> </li>
	                        		<li><p>You provide us with your current membership database in excel or csv format.  We then perform the migration of your existing database into membes.</p> </li>
	                        	</ol>
	                    	</p>
                        </div>
				  	</li>

				  	<li>
						<div class="circle-number">4</div>
						<div>
	                        <h3>Go Live</h3>
	                        <p>
	                        	Once you are happy that the everything is as it should be and ready to go.  We turn your sparkly new membes platform on for your members, and the world to see.  There are many variables effecting how long it takes to get to this stage, but usually 2-4 weeks is a fair estimate.
	                    	</p>
                        </div>
				  	</li>


				</ul>		
			</p>

        </div>
    </div>
</div>

</cfoutput>

