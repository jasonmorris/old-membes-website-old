<cfoutput>

<div class="page-banner page-banner-mobile">
	<h2>Mobile App Engagement</h2>
</div>

<div class="page-intro">
    <div class="title-section">
        <div class="container">
        	<div class="text-holder">
            <h1>Mobile App vs. Mobile Responsive</h1>
            <p>Many organisations seem like they’ve only just crawled out of the dark ages by moving away from spreadsheets to now, perhaps something a little more automated and cloud-based. You’ve probably just discovered how targeted you can be with emails once you pool all your member data into a centralised and holistic system.</p>
            <p>However, email isn’t the only tool, especially for younger users. More and more the younger generation are buried in their phones, checking social media, downloading apps or playing games but avoiding newsletters or ‘spamy’ emails. Modern AMS platforms like Membes can engage with Membes across a variety of mediums including email, SMS or through your own branded Association Mobile App.</p>
            </div>
           
            <p>&nbsp;</p>
 
         </div>
    </div>
</div>
<div class="services-box3">
	<div class="container text-left">
            
			<h3>Complete Communication Strategy</h3>    
            <div class="text-holder">     
            <p>Membes Mobile App for your association is part of communication strategy to target and engage with your members across a wide range of mediums with the Membes platform includes:</p>
            <ul class="modernlist ticklist">
 				<li><strong>Mobile responsive web content</strong></li>
                <li><strong>Targeted emails</strong></li>
                <li><strong>Custom Online Forms</strong></li>
                <li><strong>Online Surveys</strong></li>
                <li><strong>Mobile SMS</strong></li>
                <li><strong>Mobile App with Push Notifications</strong></li>
 			</ul>
            </div>
	</div>
</div>

<div class="page-intro">
    <div class="title-section">
        <div class="container">
             
            <div class="row flexy">
                <div class="col-md-8">
                    <h1>Deliver more value to your Members</h1>    
                    <div class="text-holder">  
                    
                    <p>Members can download their own mobile app for your association, update their profile information, post to forums but also receive push notifications directly to their mobile around the latest association news or events with even opening their inbox. Opening up a whole new level of communication and how you can engage with your members.</p>
                    <ul class="modernlist generallist">
                        <li>Member Login </li>
                        <li>Membes Update Details </li>
                        <li>News </li>
                        <li>Events list </li>
                        <li>Public Forum </li>
                        <li>Push Notifications on News, Events, Forum </li>
                        <li>On-going App updates </li>
                        <li>Groups Portal – view / reply (coming) </li>
                    </ul>
                    
                    </div>
                </div>    
                <div class="col-md-4">
                    <div class="img-holder" style=" text-align: center">
                        <img src="/includes/img/content/mobile-app.png" alt="Membes Branded Mobile App for Professional Associations" class="img-responsive" style="max-width: 350px; margin: 0 auto; width:100%;">
                    </div>
                </div>
            </div>
            
            <div class="text-holder">  
                <p>&nbsp;</p>
			<p>Available for Apple iOS and Android</p>
                <p>&nbsp;</p>
            </div>
  			
        </div> 
    </div>
</div>

<div class="tempcore-line">
    <div class="container">
        <a href="/demo/" style="width: 220px">Request FREE Demo</a>
        <p><i class="fa fa-thumbs-up"></i>Future Proof your Association with current best practice in technologies and standards.</p>
    </div>
</div>


</cfoutput>   