
<style>
    .video-container {
        position: relative;
        padding-bottom: 53.25%;
        padding-top: 35px;
        height: 0;
        overflow: hidden;
    }
    .video-container iframe {
        position: absolute;
        top:0;
        left: 0;
        width: 100%;
        height: 100%;
    }
</style>



<cfoutput>

<div class="page-intro">
    <div class="title-section">
        <div class="container" style="text-align: center; padding-left:30px; padding-right:30px">

            <h1>Membership Management Software that works out of the box!</h1>
			<br>
			
            <div class="video-container">
            	<iframe src="https://player.vimeo.com/video/169498020?autoplay=1&loop=1" width="100%" frameborder="0"></iframe>
            </div>
            

			<div class="row" style="padding-top: 40px; padding-bottom: 40px">
                <div class="col-md-6">
                    <a href="/about/membership-software" class="btn btn-info otherbtn">
                        Full list of Features
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i><i class="fa fa-angle-double-right" aria-hidden="true"></i><i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    </a>
                </div>
                <div class="col-md-6">
                    <a href="/demo/" class="btn btn-success otherbtn">
                        Request your FREE Demo
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i><i class="fa fa-angle-double-right" aria-hidden="true"></i><i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    </a>
                </div>
			</div>

        </div>
    </div>
</div>

</cfoutput>

