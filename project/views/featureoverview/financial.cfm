

<p>It is expected now that services can be paid for online, any time and in real time.  Taking and managing online payments in a way that is convenient for members and administration staff is now a must have.</p>

<br>

<p>
	<ul class="fa-ul" style="font-size:20px;">
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Secure and Simple online payments
	  	<p>Online payments are made easy for members and stakeholders.  Using SSL for data encryption, and direct integration to all major gateways means you don't need to store credit card details for even a second.  This ensures the highest level of security with online payments.
	  </li>
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Integrated with all major payment gateways
	  	<p>Integration into all the major payment gateways means that when your members and stakeholders pay for something using their credit card, that money is transferred straight into your Bank Account, regardless of who you are banking with.</p>
	  </li>
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Bank Reconciliation
	  	<p>Export all online payments to a format that works with all major banks and accounting software, making reconciliation a quick and easy task.</p>
	  </li>
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Integration to Accounting Packages
	  	<p>membes allows you to export all payment data in a format that can be imported directly into all major accounting packages.</p>
	  </li>
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Invoice Generation
	  	<p>All online payments generate an invoice that either includes payment methods, or indicates payment has been received.  Invoices can be viewed by date range, individual Profile and Members can even view and download all historical payment invoice via their Secured Profile login.</p>
	  </li>
	</ul>

</p>
