

<p>membes comes with a full featured, modern and customisable website and website CMS built right in.  No need to pay for a separate website, or integrations.. it's all just there.</p>

<br>

<p>
	<ul class="fa-ul" style="font-size:20px;">
	<li>
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Can be skinned to carry your brand
		<p>The public facing websites design can be customised and tailored in any way needed to accurately carry and reflect your brand and brand message.</p>
	</li>
	<li>
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Full suite of member and stakeholder functions
		<p>Everything your members and stakeholders need to interact better with you, and each other.  Events Calendars and online registration, online joining and renewal, discussion forums, SIG portals, online shop, news & events, employment listings, directories.. and more coming every day.</p>
	</li>
	<li>
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Fully integrated into Back Office data and workflows
		<p>It comes with membes is fully integrated, which means it is a part of, and has access to, all your back end data and process.  Profiles, events, training, groups.. everything about the structure of your organisation is automatically reflected in your website.</p>
	</li>
	<li>
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Compatible on all Devices, in all locations
		<p>Developed in fully compliant HTML5 and CSS3 as well as using the modern best practice of 'Mobile First' ensures the website fully functional and compatible on all devices.  Mobile Phones, tablets, desktops, touch screens and anything else that may come along in the next decade or so.</p>
	</li>
	<li>
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Powerful, yet very easy to use CMS
		<p>All pages and content is easily managed and maintained by Admin staff via the purpose built CMS.  Easily manage pages, content, banners, common snippets, modules, SEO, access rights and more</p>
	</li>
	<li>
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Search Engine Optimisation (SEO)
		<p>Compliant HTML5 and CSS3, the website is Search Engine Optimised out of the box.  All you need to do is add content, and <b>membes</b> will place the right content in the right places to ensure Search Engines accurately scan and index your site and pages.</p>
	</li>
	
	</p>
