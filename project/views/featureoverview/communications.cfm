

<p>Communicate with your members in the way they prefer, via the channels they prefer, on the devices they prefer</p>

<br>

<p>
	<ul class="fa-ul" style="font-size:20px;">
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Direct Email
	  	<p>Send a Broadcast email to your entire membership or stakeholder datbase, or a target section of your datbase, within minutes.  Or, just send an email to an individual Profile.</p>
	  </li>
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Broadcast SMS
	  	<p>Send a Broadcast SMS to your entire membership or stakeholder datbase, or a target section of your datbase, within minutes.  Or, just send an SMS to an individual Profile.</p>
	  </li>
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Communication Recording
	  	<p>All communications are logged and saved so you can always go back and check exactly what was sent, when and by who.</p>
	  </li>
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Powerful Segmentation
	  	<p>Direct target your communications by Geographic Location, Membership Type, Status, Special Interests and more.</p>
	  </li>
	   <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Social Media
	  	<p>Simplify your Social Media Communications by posting to and tracking your Facebook and Twitter postings from one place.</p>
	  </li>

</p>
