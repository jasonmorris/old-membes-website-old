

<p>
	Manage your Members, Prospective Members, Board, Suppliers, Committees and other stakeholders all from the same database.  Accessible from anywhere, on any platform or device.
</p>

<br>

<p>
	<ul class="fa-ul feature-overview-list">
		<li>
			<i class="fa-li fa fa-check-square"></i> Manage your Members
			<p>View and Edit member details with ease.  Profile history, contact log, invoices, SIG memberships, emails/SMSs sent, merge letters and more.  Everything you need to deliver quality service to your members is at your fingertips.</p>
	  	</li>
		<li>
			<i class="fa-li fa fa-check-square"></i> Track every change
			<p>Every time a Profile field is changed, membes logs what changed, its value before and after the change, and who changed it.</p>
	  	</li>
	  	<li>
			<i class="fa-li fa fa-check-square"></i> Manage Committees, Suppliers and Other Stakeholders
			<p>Manage your Associations entire stakeholder base from one place.  Contact Log, payments, communications and more.</p>
	  	</li>
	  	<li>
			<i class="fa-li fa fa-check-square"></i> New Members Join and Pay Online
			<p>New members can easily Join and Pay online.  As soon as they Join, a profile is created, with prominent alert for you to easily accept or decline their application.</p>
	  	</li>
	  	<li>
			<i class="fa-li fa fa-check-square"></i> Membership Renewal Reminder Notices
			<p>Membership Renewal reminder notices with a link to a simple Online Renewal and Payment form.  Easy access to renewal statuses for entire database as well as individual profiles. </p>
	  	</li>
	  	<li>
			<i class="fa-li fa fa-check-square"></i> Customisable Fields
			<p>Capture and store what you need and how you need it with fully customisable Profile fields. </p>
	  	</li>

	  	<li>
			<i class="fa-li fa fa-check-square"></i> Manage Duplicate Profiles
			<p>Membership Renewal reminder notices with a link to a simple Online Renewal and Payment form.  Easy access to renewal statuses for entire database as well as individual profiles. </p>
	  	</li>

	  	<li>
			<i class="fa-li fa fa-check-square"></i> Powerful filtering and segmentation
			<p>Communicate and report on your membership in a very targeted way.  Filter on Membership type & status, state, country, SIGs and more</p>
	  	</li>

	  	<li>
			<i class="fa-li fa fa-check-square"></i> Export to Excel or Labels
			<p>Using the filtering available, export profiles, stakeholders and suppliers to excel for use in other systems, mail merge or whatever other system you need to use it with.  Or, you can export direct to labels for printing</p>
	  	</li>
	
	</ul>

</p>
