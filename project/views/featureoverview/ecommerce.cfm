

<p>Allow your members to get what they need, when they need it, without having to wait for forms or payments to be processed and checked.</p>

<br>

<p>
	<ul class="fa-ul" style="font-size:20px;">
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Online Payments
	  	<p>Take online payments, approved immediately, with funds automatically transferred directly into your account by end of day.</p>
	  </li>
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Safe and Secure
	  	<p>As members integrates directly into the major gateways, there is no need for you to take or store credit card details.  Removing any risk of a breached system or PR disaster. </p>
	  </li>
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Online Shop
	  	<p>Sell merchandise, aids and supplies to your members or the general public. With member only prices, bulk purchase discounts, invoice generation and order processing all built in.</p>
	  </li>
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Event Registration Payment
	  	<p>Stakeholders can use their credit cards to pay for event registrations, so they are processed and confirmed on the spot.</p>
	  </li>
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Membership Payments
	  	<p>Payments processed immediately when members join or renew, removing need for back office processing and approvals.</p>
	  </li>
	  
	  

</p>
