

<p>Groups such as The Board, Committees, Special Interest Groups, Working Groups and more can discuss and exchange data, information and ideas in a secure and sandboxed environment.</p>

<br>

<p>
	<ul class="fa-ul" style="font-size:20px;">
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Secure and Private
	  	<p>Only membes of a group can view a group, and can only be accessed by logging in.  When a user logs in, they can only see groups that you have made them a member of.</p>
	  </li>
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Accessible any time, on any device
	  	<p>Committee members can upload and access documents anywhere, on any device (desktop, table and mobile).  Great for quickly accessing a meeting agenda at the airport, or policies while sitting in a meeting.</p>
	  </li>
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Create your own groups
	  	<p>You can create whatever groups you need, and however many you need.  There is no limit.  You set who can access a group via their Profile.</p>
	  </li>
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Group Only File Repository
	  	<p>Members of your group can upload and access files relating to their group.  Policies, meeting minutes, guides.. whatever.  It works just like a shared folder.</p>
	  </li>
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Group Only Events Calendar
	  	<p>Publish events to a groups calendar. These events are then viewable only by members of that group, and even register online if event requires registration.  </p>
	  </li>
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Group Only Discussion Forums
	  	<p>A place only visible by members of that group to discuss whatever they need to discuss.  Like any other forum, but visible only to that groups members.</p>
	  </li>
	  
</p>
