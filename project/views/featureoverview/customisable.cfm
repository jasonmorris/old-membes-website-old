

<p>Every Association is different.  It's important that the system you manage your association on is in synch with your membership structures, internal processes and operational requirements.  membes SaaS is highly customisable, with membes Standalone allowing full and unlimited customisation.</p>

<br>

<p><b>The full list of customisation options are too big to list here, but here are some:</b></p>

<br>

<p>
	<ul class="fa-ul" style="font-size:20px;">
	<li>
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Website style, pages and content
	</li>
	<li>
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Customise Member Types and Status
	</li>
	<li>
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Membership Cycles
	</li>
	<li>
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Renewal Cycle Type (rolling or periodic)
	</li>
	<li>
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Content and Signatures of system generated emails
	</li>
	<li>
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Create your own Profile Fields 
	</li>
	<li>	
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> CPD Activity Types and Cycles
	</li>
	<li>	
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Forum Categories
	</li>
	<li>	
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Profile event types
	</li>
	<li>	
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> SIGs, Committees and other groups
	</li>
	<li>	
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Profile Letter templates
	</li>
	<li style="font-size:15px;">	
		... and much more for membes SaaS, and the sky is the limit for membes Stand Alone
	</li>
</ul>
</p>
