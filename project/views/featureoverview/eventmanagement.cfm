

<p>
	Publish and manage Public and Member Only events and event registrations.  Real time CC payment, CPD Tracking Integration and comprehensive Registration Management.
</p>

<br>

<p>
	<ul class="fa-ul feature-overview-list">
		<li>
			<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Publish events to a Public, Member Online or SIG Only calendar
			<p>Easily publish and manage events and registrations to the general public, your members or an SIG group</p>
	  	</li>
	  	<li>
			<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Online Registration for Member, General Public or Trade
			<p>Attendants can register Online, any time via an Online Registration Form.  Payment can be Credit Card or Cheque, with a confirmation email and invoice being emailed immediately after registration.</p>
	  	</li>
	  	<li>
			<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Non Members become Prospective Members
			<p>Non Members automatically become prospective members, and flow into your workflows that will over time convert more prospective members into members.</p>
	  	</li>
	  	<li>
			<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Automatically disable registration when event cap is reached
			<p>Avoid overbooking.  You can specify the maximum number of attendants to an event.  When this is reached, applications are automatically disabled.</p>
	  	</li>
	  	<li>
			<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> CPD Points recorded direct to Members CPD Log
			<p>When a member registers online and their attendance to the event has been confirmed, a CPD Activity Log is automatically written to that Members CPD Log.</p>
	  	</li>
	  	<li>
			<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Tailor pricing (or free) based on Membership Type
			<p>Set different pricing for different Membership Types.  To avoid pricing mixups, the appropriate pricing is applied automatically when a member logs in based on their membership type in the system.
	  	</li>
	  	<li>
			<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> View/Export Registrations
			<p>You can view, sort, search and edit all current registrations, as well as export registrations to Excel for use in other programs such as Mail Merging or Event Management systems.</p>
	  	</li>

	  	
	
	</ul>

</p>
