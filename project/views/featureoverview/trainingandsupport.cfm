

<p>It's one thing to have a great system, but you then need to know how to use it to it's fullest potential.  To ensure you get the most out of membes, we have teamed up with leaders in the Association space such as Association Professionals (<a href="http://www.associationprofessionals.com.au" target="_blank">associationprofessionals.com.au</a>) and Morgo Online (<a href="http://www.morgoonline.com.au/" target="_blank">morgoonline.com.au</a>) to provide you with top quality training and support.</p>

<br>

<p>
	<ul class="fa-ul" style="font-size:20px;">
		 <li>
			<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Ticket based support
			<p>If you are experiencing a problem with membes you will be able to log a ticket.  Depending on this issues severity we will usually respond to within 24 hours.  This is available to all administration users of membes.</p>
		</li>
		<li>
			<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> A range of training and support packages
			<p>Depending on your needs, experience and technical knowhow, Association Professionals can provide more extensive training and support packages to suit your needs.</p>
		</li>
		<li>
			<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Phone and Email based support
			<p>Subject to the support package you take up, support or help when you get stuck or need some advice is only a phone call or an email away.</p>
		</li>
	</ul>
	 

</p>
