

<p>A public or private directory of members or other stakeholders.  Users can search for members by geographic location just by entering their postcode and a distance range</p>

<br>

<p>
	<ul class="fa-ul" style="font-size:20px;">
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Opt In/Out
	  	<p>Administration can easily include a profile in the directory via the Profile page, or members can opt to be included or excluded in the directory themselves.</p>
	  </li>
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Up to date
	  	<p>The directory is always kept up to date.  Whenever a member renews, or just updates their details, these details are automatically reflected in their directory listing.</p>
	  </li>
	   <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Directory Search
	  	<p>Users can easily find members or suppliers in their area by just entering their postcode, and what distance they are prepared to travel.  The Directory will then show them the listings closest to them.</p>
	  </li>

</p>
