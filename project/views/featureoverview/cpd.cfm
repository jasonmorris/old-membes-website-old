

<p>A powerful and fully customisable CPD Logging and Tracking tool.  Members can easily track and view CPD progress and status.  Easily monitor and Audit your membes CPD Activities.</p>

<br>

<p>
	<ul class="fa-ul" style="font-size:20px;">
	<li>
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> CPD Tracking
		<p>Members can log their CPD Activities to an online Centralised logging system.  Accessible via desktop, tablet or mobile.</p>
	</li>
	<li>
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Status at a glance
		<p>The CPD Tracker automatically tells members how many valid points they have accrued, for which CPD Cycle, and for which Activity types. It even tells them how many points they still need in each activity type.</p>
	</li>
	<li>
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Search and Print
		<p>Members can easily navigate through their logs with searching tools, view historical cycles, and print out a summary of all logs for a cycle.</p>
	</li>
	<li>
		<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Auditing
		<p>A comprehensive set of Administration tools are available to facilitate CPD ongoing and/or periodic Audits of members.</p>
	</li>
	
</p>
