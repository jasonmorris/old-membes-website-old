

<p>An Association is all about common objectives, and working towards the improvement of the profession it represents.  Effective collaboration between Board, Executive, Members and broader stakeholders impacts greatly on the quality of outcomes achieved.</p>

<br>

<p>
	<ul class="fa-ul" style="font-size:20px;">
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Easy to access website
	  	<p>The website, accessible on any device and very easy to navigate is the foundation for effective collaboration between all stakeholders.</p>
	  </li>
	  
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Discussion Forums
	  	<p>A platform for members and stakeholders to ask each other questions, share ideas and discuss topics of interest relating to your industry.</p>
	  </li>
	  
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> File Repositories
	  	<p>Committees, SIGs and other groups have a central repository to access and share files.  Documents, presentations, images, whatever can be shared amongst the group with ease.</p>
	  </li>
	  
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> Group Portals
	  	<p>Create any number of groups within your organisation, and giving them tools to communicate and share information.</p>
	  </li>
	  
	  <li>
	  	<i class="fa-li fa fa-check-square" style="color:yellowgreen"></i> CPD
	  	<p>Easily log, track and Audit CPD credits earned. In some cases, such as Event Registration, CPD points are logged automatically. </p>
	  </li>
	

</p>
