
<cfoutput>

<div class="page-banner page-banner-demo">
	<h2>Let us show you how membes can<br><span>upgrade your Associations Digital Capabilities</span>.</h2>
</div>

<cfif structKeyExists(rc,'sent')>	
	

	
	<div class="alert alert-success" role="alert">
		<strong>Thank you for your Demo Request.</strong> We will contact you shortly to arrange a time that suits you.
	</div>

	<!-- Google Code for Demo Request Submission Conversion Page -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 878391255;
	var google_conversion_language = "en";
	var google_conversion_format = "3";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "fO27CMrBp2wQ19_sogM";
	var google_remarketing_only = false;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/878391255/?label=fO27CMrBp2wQ19_sogM&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>

	<!--- Facebook --->
	<script>
	fbq('track', ‘DemoRequest’);
	</script>

</cfif>
	

<div class="container">

		<div class="row" style="padding: 100px 0;">

			<div class="col-md-3">

			</div>

			<div class="col-md-6">

				<div class="well bigbluebox">

					<h3>Request a FREE Demo.</h3>

					<form id="contact-form" class="demo-request" action="/demo/senddemorequest" method="post">

						<div class="form-field">
							<label>Full Name</label>
							<input name="name" id="name" type="text" placeholder="" class="required" autofocus>
						</div>

						<div class="form-field">
							<label>Email</label>
							<input name="email" id="email" type="text" placeholder="" class="required">
						</div>

						<div class="form-field">
							<label>Phone</label>
							<input name="phone" id="phone" type="text" placeholder="">
						</div>


						<div class="form-field">
							<label>Website</label>
							<cfif structKeyExists(form,'website')>
								<input name="website" id="website" type="text" value="#form.website#">
							<cfelse>
								<input name="website" id="website" type="text" placeholder="">
							</cfif>

						</div>

						<input type="submit" class="main-button" value="Submit Demo Request">

						<div id="msg" class="message"></div>


					</form>

					<div style="color:##fff; text-align: center; font-size: 18px; padding: 20px 10px">Or call us on 1300 377 900</div>

				</div>

			</div>

			<div class="col-md-3">

			</div>
			
	</div>

</div>

</cfoutput>


