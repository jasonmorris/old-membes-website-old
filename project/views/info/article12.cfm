
<div class="page-banner page-banner-info">
	<br><h2>Membes SaaS</h2>
</div>


<div class="page-intro">
	<div class="title-section">
        <div class="container">
        	<div class="text-holder">
            <div class="row">
            <div class="col-md-8">
            
            
            
            <h1>Secure Member Data - Enterprise Solution</h1>
            <div class="leadin">
            <p>Features. Requirements. Tick. Tick.</p>
			</div>
 			
 			
 			
			<p>Now that you’ve established your requirements and compared apples with apples amongst software features, have you investigated the supporting and underlying infrastructure?</p>
 
			<p>A good enterprise software solution is supported by good enterprise infrastructure and support team. Often the overlooked piece of the puzzle until something goes wrong or perhaps for some of us that have been burnt in the past now know to look out for. A complete enterprise solution has reliable, scalable architecture to support your association as you grow, whether you have 250 members, 2,500 members or 25,000 members, along with rigorously tested redundancy and security measures.</p>
 
 			<div class="row highlights">
            	<div class="third">
            		<div class="article-circle"><span class="glyphicon glyphicon-time"></span></div>
           			Scalable performance<br>and infrastructure
           		</div>
            	<div class="third">
            		<div class="article-circle"><span class="glyphicon glyphicon-signal"></span></div>
            		Australian<br>data location
            	</div>
            	<div class="third">
            		<div class="article-circle"><span class="glyphicon glyphicon-wrench"></span></div>
            		Improved security,<br>stability and redundancy
            	</div>
            </div>
 			
			<p>Membes utilises one of the leading global companies for infrastructure and support. While data is kept within Australian shores, servers and infrastructure are monitored 24/7 by experienced technical staff available regardless of time zone or business hours.</p>
 
			<p>Ensuring you have peace of mind we also keep extensive backups - retaining and backing up your data daily for 14 days, then monthly for 12 months.</p>
        
            
            
 			
          <p>&nbsp;</p>
           
            
            </div>
        	
            <div class="col-md-4">
            <div class="sidebox">
            <h3 align="center">Have you booked a demo yet?</h3>
            
            	<form id="contact-form" class="demo-request" action="/demo/senddemorequest" method="post">

						<div class="form-field">
							<label>Full Name</label>
							<input name="name" id="name" type="text" placeholder="" class="required" autofocus>
						</div>

						<div class="form-field">
							<label>Email</label>
							<input name="email" id="email" type="text" placeholder="" class="required">
						</div>

						<div class="form-field">
							<label>Phone</label>
							<input name="phone" id="phone" type="text" placeholder="">
						</div>


						<div class="form-field">
							<label>Website</label>
							<cfif structKeyExists(form,'website')>
								<input name="website" id="website" type="text" value="#form.website#">
							<cfelse>
								<input name="website" id="website" type="text" placeholder="">
							</cfif>

						</div>

						<input type="submit" class="main-button" value="Submit Demo Request">

						<div id="msg" class="message"></div>


					</form>
        	</div>
			</div>
            
            </div>
            </div>  
        </div>
    </div>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
