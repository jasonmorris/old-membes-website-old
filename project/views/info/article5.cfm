
<div class="page-banner page-banner-city">
</div>


<div class="page-intro">
	<div class="title-section">
        <div class="container">
        	<div class="text-holder">
            <div class="row">
            <div class="col-md-8">
            
            
            
            <h1>Association Software Pitfalls - Making the Right Choice</h1>
            <div class="leadin">
				<p>The IT department and software contractors are often one of the most hated departments or organisation suppliers. Time and time again software projects run overtime, costs always seem to be higher, and the delivered functionality often isn’t what you expected. The software landscape can vary between industries, but it is still a common challenge whether you work in the commercial or non-profit sectors.</p>

				<p>However, many software project dramas don’t lie in your contractor (though there are some shady operators out there!). More often than not, the problem lies in the choice software and/or the delivery method of this software. If you were in a totally different industry and you happened to be a building contractor looking to purchase a fleet of work vehicles, would you choose a set of hardy work trucks with proven haulage capabilities or a set of two-seater shiny sports that can go from 0-100 in 3 seconds? So, if you’ve asked for a square peg to fit in a round hole, don’t blame your IT guy!</p>
				<p>&nbsp;</p>
			</div>
			
			<h3>Why Bespoke Software can be challenging</h3>
			<p>Software is a tool, and like many tools, you need to select the right one for the task at hand. Bespoke software, which has been the traditional approach for many years for associations building a CRM, database or website solution (mainly because alternatives had yet to evolve). Bespoke has its pros but also its cons. When considering bespoke software, you need to understand it’s nature. </p>

			<p>A major advantage of bespoke software is its increased freedom and flexibility to be built to solve almost any task, provided you have the necessary budget and don’t cut corners. A common failure in many bespoke software projects typically comes from the lack of appropriate scoping and analysis. Just as if you were building a new house but had incomplete or inaccurate plans you are going to run into major problems and likely resulting in a failed project. If a client is expecting gold plated fixtures and the builder assumes silver, the client will need to accept the compromise or somebody must pay the difference in the costs of materials/work to replace the fixtures. </p>

			<p>There may be justifiable reasons and internal processes of the organisation that require taking the bespoke path, but if the reasons for going bespoke can be avoided or worked around there is a considerably more suitable alternative that can save a lot of time and money as well as maximise chances of a successful outcome. The cost and time savings between a bespoke solution and an off-the-shelf Software as a Service (SaaS) platform could be factors of 10, 20 or even more. This begs the question, if you are at least a close fit to a SaaS alternative can you tweak your internal processes so that it’s ‘close enough’ allowing your organisation take advantage of the huge list of advantages that come with taking the SaaS path?</p>
			<p>&nbsp;</p>
				
			<h3>Economies of Scale with (true) Software as a Service (SaaS)	</h3>
			<p>Software as a Service or ‘SaaS’ is a term that is increasingly thrown around by software vendors these days. SaaS is sometimes associated with subscription based pricing models which in itself can be very appealing especially with the reduced capital expenditure compared to traditional models. However, SaaS is more than just a pricing model but a technical approach to software design, infrastructure and delivery methods that leverage economies of scale. Hence, the large reductions in cost and time to implement, maintain and upgrade.</p>

			<p>Increasingly, modern software is being developed to meet the needs of a particular industry ‘niche’ containing workflows optimised to make life easier for people associated to that industry, as opposed to generic, widely adopted software that might appeal to a broader set of use cases. Within this ‘niche’, the vendor would have identified common administrative challenges and designed the platform to meet these needs along with a variety of configurable settings to accommodate variations of these requirements across similar organisations. </p>

			<p>New features are each carefully considered to give the best value to the pool of likewise organisations currently using the platform. Typically, a feature that may save an administrator 30 minutes every day is going to be prioritised over features that will only save 30 minutes a month. The result of the niche SaaS platform is a software solution that is unbeatable value for money.</p>

			<p>Because SaaS platforms are off-the-shelf as opposed to being built to order, implementation times are extremely quick, development costs are effectively shared across a multiple of clients rather than a single client and IT infrastructure can be leveraged across a larger network of users.</p>
			<p>&nbsp;</p>
				
			<h3>Separate software stuck together to try and be one</h3>
			<p>As we all know technologies evolve very fast. Cloud computing, Big Data, Machine learning, Artificial Intelligence. It’s no wonder it’s hard to keep track of emerging opportunities and alternative solutions. Still, many of us use systems or approaches we’ve been familiar with for years.

			<p>Many organisations still use an overly abundant number of different software systems. You can find organisations utilising separate systems for membership, events, communications, web publications, file sharing and more. Sometimes, there can exist 5 or 6 separate software systems that could easily be replaced by a single software platform designed specifically for that industry’s needs. </p>

			<p>Reducing the number of software components may have a direct reduction in operating costs, though the reduction in complexity or risk can be more significant. Between the multiple systems there may or may not be integrations to share data. Where there are integrations, these need to be maintained. Where there are not, you can expect manual processing and inefficient administration practices.</p>

			<p>Another problem that can arise when running an organisation on multiple systems, is that the complex web of software does not get upgraded for fear of breaking the established data sharing and integrations. This in turn presents a potential security risk, especially with new data breach/loss regulations recently introduced in Australia (Privacy Amendment Act 2017 and Notifiable Data Breaches scheme) where organisations can incur penalties of $2.1 million per security breach.</p>

			<p>Combining common association software into a more holistic platform can reduce direct costs, increase data integrity and can significantly leverage existing data to reduce manual processing, allow further targeted communications and improve reports and analytics. Platforms that are purpose built for Associations and their specific tasks and needs are referred to as Association Management Software (AMS) platforms.</p>

			<p>The benefits of using a holistic AMS platform over a suite of different software systems will vary between association but in all cases will reduce manual processing of staff and administration time by as much as 85 percent. It is very clear the cost savings will eclipse any initial or ongoing costs of an appropriate AMS.</p>
			<p>&nbsp;</p>
				
            <h3>Embrace Open Eco-systems, and eliminate vendor service monopolies</h3>
			<p>One of the reasons overall software costs are so high are all those little random and unexpected costs requiring you to continually go back to your provider for tweaks and changes. Most providers are often setup where after purchasing the initial software for any ongoing changes you only have one choice to make these changes, and that’s through the initial provider. Effectively creating a service monopoly.</p>

			<p>For associations, the tweaks could include updating banners, page layouts, menus, adding new directories or services, Membership Types, Events and so on, or perhaps even a complete site design and rebranding. When the control sits with the software vendor, small changes can easily cost thousands of dollars or even tens of thousands of dollars for seemingly small adjustments and enhancements.</p>

			<p>An emerging alternative to Closed Eco-system software is Open Eco-system software which give clients more control over their content and software configuration to make the choices themselves, or with more open access to make changes with pre-existing designers and technicians of their choosing. Once you eliminate the single service monopoly and increase options for service providers you create a more competitive landscape with lower prices, increased choice and increased accessibility to more expertise.</p>
			<p>&nbsp;</p>
				
			<h3>Cost of upgrade disruption versus an always evolving platform</h3>
			<p>Custom Software has a limited shelf life (generally 5 years is considered a standard life span). We know at some point in the future, your website, database or other software will start showing its age or there might be new alternatives that deliver significantly better value. Forcing you to start the process of a major upgrade or replacement – perhaps you are doing that right now.</p>

			<p>We also know, especially in more recent times, technologies not only evolve but so do the models on which they are delivered. We call these market disruptors. For example, it wasn’t so long ago you could purchase a single music CD at the store for $27 compared to today being able to pay $10/month for access to tens or hundreds of thousands of songs streamed directly to a device in your pocket whether you are at home, on the bus or about to board a plane.</p>

			<p>Modern software design principles created similar disruptions. </p> 
			
			<ol>
			<li><p>With economies of scale, software value for money increases significantly – which in most cases, eliminates the need for costly bespoke built and maintained projects. </p></li>

			<li><p>More elegant software design along with economies of scale infrastructure effectively reduces development time/cost to deliver a more progressive and continuously evolving platform which eliminates major upgrade cycles. </p></li>
			</ol>
				
			<p>So, for platforms that never need replacing that evolve to increase functionality and user efficiency the ROI is completely disrupted versus limited shelf-life software.</p>
			
			<p>&nbsp;</p>
			
			<h2>Other Considerations</h2>
			<br>
				
			<h3>Choosing the right SaaS platform</h3>
			<p>There is no doubt that modern SaaS platforms are a game changer in terms of value for money. However, make sure you get the right platform for the task at hand. There are many generic platforms, often orientated towards sales and lead tracking, but not necessarily designed to accommodate associations or member-based non-profits.</p>

			<p>This can lead to two outcomes. Platforms that are too broad in nature, or designed for an alternative market or purpose (such as sales orientated CRMs) that won’t effectively delivery on desired outcomes suited to Associations or won’t deliver the best value for your organisation. The workflows of a CRM are geared more to selling and lead tracking, not membership management and engagement.</p>
			
			<p>&nbsp;</p>
				
			<h3>Talk to an Expert</h3>
			<p>Not sure what is the right solution for your organisation’s needs? Find an expert. There are many consultants within the Association space that can assist with ensuring you make the right choices for you. Find an expert who has experience implementing solutions similar to your organisations structure, size and goals. Just because a board member’s daughter has experience with rolling out Salesforce or once built a site on WordPress, doesn’t mean they might be the most appropriate person for implementing your next project.</p>
			<p>&nbsp;</p>
				
			<h3>Are you really getting what you asked for?	</h3>
 			<p>Sometimes the loathing of past IT ‘professionals’ might be entirely justified. As with most industries, we have our shady characters. Which unfortunately for customers, you don’t always get what you pay for. Thought you were getting SaaS but are you really getting bespoke?</p>

			<p>Because SaaS is more than just a pricing model, but a set of software and infrastructure design principles it can sometimes be difficult at face-value to really know what you are signing up for until it’s too late. However, there are some tell-tell signs. </p>

			<p>There are two key indicators:</p>
			
			<ol>
			<li><p>Time required: Bespoke software needs to be built and hence typically has a much longer implementation time which would usually run into several months. SaaS is ‘configured’, so implementation can be almost immediate or only a few weeks. So an AMS taking 6-12 months to setup is not SaaS, but very likely Bespoke. A SaaS AMS will usually be ready for you to start working on within weeks.</p></li>

			<li><p>Cost: A bespoke AMS (for a typical Associations) will likely cost anywhere between $50,000 - $500,000. The same level of capability can be obtained through SaaS for under $50,000.. often as low as $10-20,000</p></li>
			</ol>

				
         	<p>&nbsp;</p>
           
            
            <h3>Read More</h3>
           
		  	<ul>
				<li><a href="/info/the-difference-between-an-ams-and-a-crm">What is the difference between an AMS and CRM?</a></li>	
				<li><a href="/info/bespoke-vs-saas-association-management-software">Bespoke vs. Software as a Service (SaaS) Association Software</a></li>
	  		 	<li><a href="/info/open-over-closed-ecosystem-association-software">Importance of an Open Ecosystem over Closed Ecosystem AMS</a></li>
				<li><a href="/info/maximum-design-flexibility">Maximum Design Flexibility</a></li>
				<li><a href="/info/engage-with-your-members">Engage with Your Members</a></li>
		   	</ul>
           
            
            <p>&nbsp;</p>
            
            
            </div>
        	
            <div class="col-md-4">
            <div class="sidebox">
            <h3 align="center">Have you booked a demo yet?</h3>
            
            	<form id="contact-form" class="demo-request" action="/demo/senddemorequest" method="post">

						<div class="form-field">
							<label>Full Name</label>
							<input name="name" id="name" type="text" placeholder="" class="required" autofocus>
						</div>

						<div class="form-field">
							<label>Email</label>
							<input name="email" id="email" type="text" placeholder="" class="required">
						</div>

						<div class="form-field">
							<label>Phone</label>
							<input name="phone" id="phone" type="text" placeholder="">
						</div>


						<div class="form-field">
							<label>Website</label>
							<cfif structKeyExists(form,'website')>
								<input name="website" id="website" type="text" value="#form.website#">
							<cfelse>
								<input name="website" id="website" type="text" placeholder="">
							</cfif>

						</div>

						<input type="submit" class="main-button" value="Submit Demo Request">

						<div id="msg" class="message"></div>


					</form>
        	</div>
			</div>
            
            </div>
            </div>  
        </div>
    </div>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
