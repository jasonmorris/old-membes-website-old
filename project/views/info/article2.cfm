
<div class="page-banner page-banner-tailor">

</div>


<div class="page-intro">
	<div class="title-section">
        <div class="container">
        	<div class="text-holder">
            <div class="row">
            <div class="col-lg-8">
            
            
            
            <h1>Bespoke vs. SaaS</h1>
            <div class="leadin">
            <p>An Association Management Software (AMS) platform is designed to significantly improve administration efficiency, reduce manual processing and improve data insights for most associations or member-based organisations. However, in selecting an AMS, you have two paths you can take - bespoke or modern SaaS. Which is right for you?</p>
			</div>
 			
			<p>Both have their pros and cons. In the basic sense, bespoke allows for increased customizations through changing the underlying core code of the software. Whereas SaaS platforms adapt to your requirements using the existing configuration options without changing the underlying core code which allows leveraging economies of scale.</p>

			<p>While it may be appealing to having a granular level of control over your software with a bespoke solution, it may be unnecessary, or when slightly modifying your existing internal processes could mean the difference of using SaaS instead. The advantage of using SaaS will significantly lower or reduce the overheads compared to a bespoke system in time, costs and other resources.</p>
 			
 			<table border="0" width="100%" class="infotable">
				<thead>
					<tr>
						<th>&nbsp;</th>
						<th class="aligncenter">Bespoke</th>
						<th class="aligncenter">SaaS</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><strong>Implementation times</strong> </td>
						<td class="aligncenter">6 - 12 months</td>
						<td class="aligncenter">2 - 6 weeks</td>
					</tr>
					<tr>
						<td><strong>Implementation resources</strong></td>
						<td class="aligncenter">Intensive</td>
						<td class="aligncenter">Streamlined</td>
					</tr>
					<tr>
						<td><strong>Costs</strong> </td>
						<td class="aligncenter">$50 - $150k</td>
						<td class="aligncenter">Under $10k</td>
					</tr>
					<tr>
						<td><strong>Replacement</strong>  </td>
						<td class="aligncenter">Every 4 - 5 years</td>
						<td class="aligncenter">Never</td>
					</tr>
					<tr>
						<td><strong>Support requirements</strong> </td>
						<td class="aligncenter">High need</td>
						<td class="aligncenter">Reduced need</td>
					</tr>
					<tr>
						<td><strong>Upgrade cost</strong>  </td>
						<td class="aligncenter">Cost per enhancement</td>
						<td class="aligncenter">Included</td>
					</tr>
					<tr>
						<td><strong>Upgrade frequency</strong>  </td>
						<td class="aligncenter">As requested</td>
						<td class="aligncenter">Weekly or monthly</td>
					</tr>
					<tr>
						<td><strong>Infrastructure</strong>  </td>
						<td class="aligncenter">Basic</td>
						<td class="aligncenter">Enterprise</td>
					</tr>			
				</tbody>
			</table>
 			
 			<p>&nbsp;</p>
 			
 			<h3>When to use Bespoke Software</h3>
 			
 			<p>It's obvious that no two organisations are ever truly alike. There may be many common challenges or requirements across many associations in which a SaaS solution will aim to address. However, a SaaS solution may only meet 90 - 95 percent of your overall needs. If you have both the initial capital, and ongoing resources then closing that gap with the last 5 - 10 percent can be an option. Bespoke gives you that granular level of control, but at the trade-off of significantly increased upfront and ongoing expense.</p>

			<p>Many organisations also unintentionally pursue bespoke solutions without realising there are now more viable options available. Often, they begin with off-the-shelf software, but either initially or over time they 'tweak' the existing code to better suit their current workflows (rather than adapt internal processes to the platform), add custom modules, or create custom integrations to link the multitude of software components. This overall system becomes unique to your organisations and creates very complex software cross-dependencies making upgrading or enhancements more resource intensive (and costly).</p>

			<h3>When to use SaaS solutions</h3>

			<p>SaaS suits those organisations that don't operate with unique internal processes or don't have the budget for the upfront and ongoing development costs to sustain a bespoke solution adequately. SaaS will eliminate many of the random costs associated with bespoke or custom software that crop up due to bug fixes or feature enhancements - allowing organisations to more confidently plan and budget appropriately.</p>

			<p>Frequent enhancements with SaaS ensure continuous improvements to workflows, communication, member engagement, security and reliability. SaaS also eliminates the typical major upgrade periods compared to bespoke around the four to five year period where the system becomes out-dated or "clunky".</p>

			<h3>Conclusion</h3>
				
			<p>In the same way as you could buy a house off the plan with configuration options OR approach an architect to have a house Custom designed and built to your specific requirements. Software can now be obtained in a similar sense, with SaaS being similar to off the plan with configuration options, and Bespoke being a custom designed and developed solution. Both have their merits and costs, it is just a case of determining which is most suited to your objectives and needs.</p>	
				
 			<div class="row highlights">
            	<div class="third">
            		<div class="article-circle"><span class="glyphicon glyphicon-globe"></span></div>
           			Leverage economies<br>of Scale
           		</div>
            	<div class="third">
            		<div class="article-circle"><span class="glyphicon glyphicon-signal"></span></div>
            		Faster implementation<br>&amp; Lower Costs 
            	</div>
            	<div class="third">
            		<div class="article-circle"><span class="glyphicon glyphicon-time"></span></div>
            		More Frequent<br>Feature Enhancements
            	</div>
            </div>    
 			
         <h3>Read More</h3>
           
		  	<ul>
				<li><a href="/info/the-difference-between-an-ams-and-a-crm">What is the difference between an AMS and CRM?</a></li>	
				<li><a href="/info/open-over-closed-ecosystem-association-software">Importance of an Open Ecosystem over Closed Ecosystem AMS</a></li>
	   			<li><a href="/info/maximum-design-flexibility">Maximum Design Flexibility</a></li>
				<li><a href="/info/association-software-pitfalls">Association Software Pitfalls - Making the right Choice</a></li>
				<li><a href="/info/engage-with-your-members">Engage with Your Members</a></li>
		   	</ul>
           
            
            <p>&nbsp;</p>
           
            
            </div>
        	
            <div class="col-lg-4">
            <div class="sidebox">
            <h3 align="center">Have you booked a demo yet?</h3>
            
            	<form id="contact-form" class="demo-request" action="/demo/senddemorequest" method="post">

						<div class="form-field">
							<label>Full Name</label>
							<input name="name" id="name" type="text" placeholder="" class="required" autofocus>
						</div>

						<div class="form-field">
							<label>Email</label>
							<input name="email" id="email" type="text" placeholder="" class="required">
						</div>

						<div class="form-field">
							<label>Phone</label>
							<input name="phone" id="phone" type="text" placeholder="">
						</div>


						<div class="form-field">
							<label>Website</label>
							<cfif structKeyExists(form,'website')>
								<input name="website" id="website" type="text" value="#form.website#">
							<cfelse>
								<input name="website" id="website" type="text" placeholder="">
							</cfif>

						</div>

						<input type="submit" class="main-button" value="Submit Demo Request">

						<div id="msg" class="message"></div>


					</form>
        	</div>
			</div>
            
            </div>
            </div>  
        </div>
    </div>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<script>
    (function(w,d,t,u,n,a,m){w['MauticTrackingObject']=n;
        w[n]=w[n]||function(){(w[n].q=w[n].q||[]).push(arguments)},a=d.createElement(t),
        m=d.getElementsByTagName(t)[0];a.async=1;a.src=u;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://membes.mautic.net/mtc.js','mt');

    mt('send', 'pageview');
</script>
