
<div class="page-banner page-banner-info">
	<br><h2>Membes SaaS</h2>
</div>


<div class="page-intro">
	<div class="title-section">
        <div class="container">
        	<div class="text-holder">
            <div class="row">
            <div class="col-md-8">
            
            
            
            <h1>Integrating CPD tracking and auditing with Membership<h1>
            <div class="leadin">
            <p>How are you managing your accreditation and Continuous Professional Development (CPD) with your members? Did you know that you can integrate, track, audit and manage CPD submission from the same system as your membership?</p>
			</div>
 			
 			
 			
			<p>The traditional approach with many generic Membership or CRM systems is that they don’t take holistic considerations of your other association activities such as CPD tracking and auditing. With Membes SaaS you can start to create some automation and remove the manual processing around CPD. Whether you are vetting all submitted CPD activities or whether you audit a random selection of member accreditations at the end of each cycle, Membes SaaS gives you the tools to manage the process. Membes retains detailed CPD history against each profile in a centralised and easily accessible platform from the office or on the road with a tablet or smartphone.</p>
 
 			<div class="row highlights">
            	<div class="third">
            		<div class="article-circle"><span class="glyphicon glyphicon-time"></span></div>
           			Online CPD<br>Submission
           		</div>
            	<div class="third">
            		<div class="article-circle"><span class="glyphicon glyphicon-signal"></span></div>
            		Manage CPD/CPE<br>Activities
            	</div>
            	<div class="third">
            		<div class="article-circle"><span class="glyphicon glyphicon-wrench"></span></div>
            		Automated<br>Auditing
            	</div>
            </div>
 			
			<p>Moving beyond spreadsheets, you can track the progress of members throughout their CPD cycle, allow user submission of activities with supporting documentation, set caps on other activities and automate follow-ups. You can also flag or randomly select members for auditing, then check against your constitutional requirements.</p>
        
            
            
 			
          <p>&nbsp;</p>
           
            
            </div>
        	
            <div class="col-md-4">
            <div class="sidebox">
            <h3 align="center">Have you booked a demo yet?</h3>
            
            	<form id="contact-form" class="demo-request" action="/demo/senddemorequest" method="post">

						<div class="form-field">
							<label>Full Name</label>
							<input name="name" id="name" type="text" placeholder="" class="required" autofocus>
						</div>

						<div class="form-field">
							<label>Email</label>
							<input name="email" id="email" type="text" placeholder="" class="required">
						</div>

						<div class="form-field">
							<label>Phone</label>
							<input name="phone" id="phone" type="text" placeholder="">
						</div>


						<div class="form-field">
							<label>Website</label>
							<cfif structKeyExists(form,'website')>
								<input name="website" id="website" type="text" value="#form.website#">
							<cfelse>
								<input name="website" id="website" type="text" placeholder="">
							</cfif>

						</div>

						<input type="submit" class="main-button" value="Submit Demo Request">

						<div id="msg" class="message"></div>


					</form>
        	</div>
			</div>
            
            </div>
            </div>  
        </div>
    </div>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
