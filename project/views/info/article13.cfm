
<div class="page-banner page-banner-info">
	<br><h2>Membes SaaS</h2>
</div>


<div class="page-intro">
	<div class="title-section">
        <div class="container">
        	<div class="text-holder">
            <div class="row">
            <div class="col-md-8">
            
            
            
            <h1>Cloud Membership - Any Device, Anywhere</h1>
            <div class="leadin">
            <p>Association and member-based software have evolved from traditional spreadsheets, and desktop systems to now allow device-independent access from mobile, tablet or desktops.</p>
			</div>
 			
 			
 			
			<p>Mobile responsive user sites and administration portals allow members to access their profile data, registration for events or admin to manage their associations regardless of desktop, smartphone or tablet device. </p>
 
 			
 			<div class="row highlights">
            	<div class="third">
            		<div class="article-circle"><span class="glyphicon glyphicon-time"></span></div>
           			Mobile responsive design
           		</div>
            	<div class="third">
            		<div class="article-circle"><span class="glyphicon glyphicon-signal"></span></div>
            		More accessible data
            	</div>
            	<div class="third">
            		<div class="article-circle"><span class="glyphicon glyphicon-wrench"></span></div>
            		Modern and progressive software
            	</div>
            </div>
 			
			<p>Don’t pay extra to have a modern responsive site for your members and don’t force your admin team to use management portals that don’t keep pace with current standards.</p>
        
            
            
 			
          <p>&nbsp;</p>
           
            
            </div>
        	
            <div class="col-md-4">
            <div class="sidebox">
            <h3 align="center">Have you booked a demo yet?</h3>
            
            	<form id="contact-form" class="demo-request" action="/demo/senddemorequest" method="post">

						<div class="form-field">
							<label>Full Name</label>
							<input name="name" id="name" type="text" placeholder="" class="required" autofocus>
						</div>

						<div class="form-field">
							<label>Email</label>
							<input name="email" id="email" type="text" placeholder="" class="required">
						</div>

						<div class="form-field">
							<label>Phone</label>
							<input name="phone" id="phone" type="text" placeholder="">
						</div>


						<div class="form-field">
							<label>Website</label>
							<cfif structKeyExists(form,'website')>
								<input name="website" id="website" type="text" value="#form.website#">
							<cfelse>
								<input name="website" id="website" type="text" placeholder="">
							</cfif>

						</div>

						<input type="submit" class="main-button" value="Submit Demo Request">

						<div id="msg" class="message"></div>


					</form>
        	</div>
			</div>
            
            </div>
            </div>  
        </div>
    </div>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
