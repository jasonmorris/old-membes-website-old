
<div class="page-banner page-banner-info">
	<br><h2>Membes SaaS</h2>
</div>


<div class="page-intro">
	<div class="title-section">
        <div class="container">
        	<div class="text-holder">
            <div class="row">
            <div class="col-md-8">
            
            
            
            <h1>Industry Experienced Support &amp; Best Practices</h1>
            <div class="leadin">
            <p>We understand the right support can make or break the adoption of any major technology or software change. With Membes we have constructed a network of support staff and trainers that are not simply developers or software engineers trying to convey new software features, but industry professionals that have years of real world experience managing, consulting or advising peak organisations, institutes, associations and societies.</p>
			</div>
 			
 			
 			
			<p>The advantage for you with Membes is that not only will you be taught by industry experts, but from people that understand the common challenges you face, and offer practical or real world guidance to get the most out of your new Membes system.</p>
 
 			<div class="row highlights">
            	<div class="third">
            		<div class="article-circle"><span class="glyphicon glyphicon-time"></span></div>
           			Industry experienced<br>support staff
           		</div>
            	<div class="third">
            		<div class="article-circle"><span class="glyphicon glyphicon-signal"></span></div>
            		Relatable and<br>practical guidance
            	</div>
            	<div class="third">
            		<div class="article-circle"><span class="glyphicon glyphicon-wrench"></span></div>
            		Best practices and<br>tailored support
            	</div>
            </div>
 			
			
            
 			
          <p>&nbsp;</p>
           
            
            </div>
        	
            <div class="col-md-4">
            <div class="sidebox">
            <h3 align="center">Have you booked a demo yet?</h3>
            
            	<form id="contact-form" class="demo-request" action="/demo/senddemorequest" method="post">

						<div class="form-field">
							<label>Full Name</label>
							<input name="name" id="name" type="text" placeholder="" class="required" autofocus>
						</div>

						<div class="form-field">
							<label>Email</label>
							<input name="email" id="email" type="text" placeholder="" class="required">
						</div>

						<div class="form-field">
							<label>Phone</label>
							<input name="phone" id="phone" type="text" placeholder="">
						</div>


						<div class="form-field">
							<label>Website</label>
							<cfif structKeyExists(form,'website')>
								<input name="website" id="website" type="text" value="#form.website#">
							<cfelse>
								<input name="website" id="website" type="text" placeholder="">
							</cfif>

						</div>

						<input type="submit" class="main-button" value="Submit Demo Request">

						<div id="msg" class="message"></div>


					</form>
        	</div>
			</div>
            
            </div>
            </div>  
        </div>
    </div>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
