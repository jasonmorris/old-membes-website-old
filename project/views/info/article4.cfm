
<div class="page-banner page-banner-design">
</div>


<div class="page-intro">
	<div class="title-section">
        <div class="container">
        	<div class="text-holder">
            <div class="row">
            <div class="col-md-8">
            
            
            
            <h1>Maximum Design Flexibility</h1>
            <div class="leadin">
				<p>Membes has been designed with usability in mind but also design flexibility for you to take your website in the direction that you want.</p>
				<p>&nbsp;</p>
			</div>
			
			<h3>Single Database Approach</h3>

			<p>Membes is primarily a holistic, single database and single point of truth Association Management Software (AMS) platform to allow non-technical users easy access to update and manage their web content as well as automate the manage administrative tasks a typical association will face.</p>

			<p>As a part of this Membes includes a very capable CMS (website builder) for you to craft a feature rich public website, member's portal, and other stakeholder areas without the typical reliance on vendors when you need to make significant changes.</p>

			<p>Utilising a single database approach as opposed to connecting with a third-party website or CMS, the Membes CMS has full access to up to date member, profile or stakeholder information to automate the control of restricted access content or automatically apply things like member related discounts for events or store purchases without the need for manual intervention or verification or maintaining integrations with additional software.</p>

			<p>The flexibility of the Membes CMS whilst designed to appeal to the non-technical typical administrative user, it does have open access and the capabilities of pushing the aesthetic appeal of your site as far as you desire with custom HTML, CSS, JavaScript, linking CDN libraries or allowing embedding of external widgets. </p>
			
			<p>&nbsp;</p>

			<h3>Embedding Capabilities</h3>

			<p>Whilst the recommended approach is using Membes as a single database solution because of its extended functionality and elimination of having to maintain integrations across separate software components, Membes can work with existing websites whether they are built on WordPress, Joomla or even Wix.</p>

			<p>Membes allows for direct integration or embedding into existing websites without the need to run clunky third party services that sit on separate subdomains that can hinder user experience.</p>
			
			<p>&nbsp;</p>
			
			<h3>Membes API</h3>

			<p>In addition to all the above, Membes also allows connections to third party software or Apps via it's modern REST API. So, if you need to authenticate members via a custom mobile App or pull a list of recent events or news feed you can. Let us know your requirements and we can discuss with you to make sure you have access to the data you need.</p>


            
 			
         	<p>&nbsp;</p>
           
            
            <h3>Read More</h3>
           
		  	<ul>
				<li><a href="/info/the-difference-between-an-ams-and-a-crm">What is the difference between an AMS and CRM?</a></li>	
				<li><a href="/info/bespoke-vs-saas-association-management-software">Bespoke vs. Software as a Service (SaaS) Association Software</a></li>
	  		 	<li><a href="/info/open-over-closed-ecosystem-association-software">Importance of an Open Ecosystem over Closed Ecosystem AMS</a></li>
				<li><a href="/info/association-software-pitfalls">Association Software Pitfalls - Making the right Choice</a></li>
				<li><a href="/info/engage-with-your-members">Engage with Your Members</a></li>
		   	</ul>
           
            
            <p>&nbsp;</p>
            
            
            </div>
        	
            <div class="col-md-4">
            <div class="sidebox">
            <h3 align="center">Have you booked a demo yet?</h3>
            
            	<form id="contact-form" class="demo-request" action="/demo/senddemorequest" method="post">

						<div class="form-field">
							<label>Full Name</label>
							<input name="name" id="name" type="text" placeholder="" class="required" autofocus>
						</div>

						<div class="form-field">
							<label>Email</label>
							<input name="email" id="email" type="text" placeholder="" class="required">
						</div>

						<div class="form-field">
							<label>Phone</label>
							<input name="phone" id="phone" type="text" placeholder="">
						</div>


						<div class="form-field">
							<label>Website</label>
							<cfif structKeyExists(form,'website')>
								<input name="website" id="website" type="text" value="#form.website#">
							<cfelse>
								<input name="website" id="website" type="text" placeholder="">
							</cfif>

						</div>

						<input type="submit" class="main-button" value="Submit Demo Request">

						<div id="msg" class="message"></div>


					</form>
        	</div>
			</div>
            
            </div>
            </div>  
        </div>
    </div>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
