
<cfoutput>

<div class="statistic-section">

    <cfparam name="args.showIntro" default="true">
    <cfif args.showIntro>
        <div class="title-section">
            <div class="container">
                <h1>Membership Management Software Capabilities</h1>
                <p>membes Association Management Software comes packed with everything you need to manage your Association integrated into one seamless platform.<br> No expensive add-on modules, or custom development.  A platform that just works.</p>
            </div>
        </div>
    </cfif>
    
    <div>
        <div class="container">
        
            <div class="row">
                
                <div class="col-md-3">
                	<a href="/featureoverview/stakeholdermanagement/" class="link-box" data-toggle="modal" data-target="##modal" >
                        <div>
                            <span class="icon-stat"><i class="fas fa-user"></i></span>
                            <h2>Stakeholder Management</h2>
                            <p>Manage your Members, Prospective Members, Board, Suppliers, Committees and other stakeholders all from the same database, from anywhere, on any platform or device.</p>
                            <small>find out more... </small>
                        </div>
                    </a>
                </div>
                
                <div class="col-md-3">
                	<a href="/featureoverview/eventmanagement/" class="link-box" data-toggle="modal" data-target="##modal">
                        <div>
                            <span class="icon-stat"><i class="fas fa-calendar-alt"></i></span>
                            <h2>Event Management</h2>
                            <p>Publish and manage Public and Member Only events and event registrations.  Real time CC payment, CPD Tracking Integration and comprehensive Registration Management.</p>
                            <small>find out more... </small>
                        </div>
                    </a>
                </div>
                
                <div class="col-md-3">
                	<a href="/featureoverview/communications/" class="link-box" data-toggle="modal" data-target="##modal">
                        <div>
                            <span class="icon-stat"><i class="fas fa-envelope"></i></span>
                            <h2>Communication</h2>
                            <p>Communication one on one or Broadcast via Email and SMS, with all Communications logged for future reference.  Powerful segmentation and filtering to hone in your message.  </p>
                            <small>find out more... </small>
                        </div>
                    </a>
                </div>
				
				<div class="col-md-3">
                	<a href="/featureoverview/websitecms/" class="link-box" data-toggle="modal" data-target="##modal">
                        <div>
                            <span class="icon-stat"><i class="fas fa-desktop"></i></span>
                            <h2>Website CMS</h2>
                            <p>Fully self managed website for the Public, membes and stakeholders.  A powerful, yet easy to use CMS system that is fully integrated into all the platforms modules giving a single, coherent platform for all stakeholders.</p>
                            <small>find out more... </small>
                        </div>
                    </a>
                </div>
                
            </div>
            
            <div class="row">
                
                <div class="col-md-3">
                	<a href="/featureoverview/financial/" class="link-box" data-toggle="modal" data-target="##modal">
                        <div>
                            <span class="icon-stat"><i class="fas fa-hand-holding-usd"></i></span>
                            <h2>Financial</h2>
                            <p>Real time CC payments, or manage cheque processing.  Integration to Payflow Pro, Secure Pay and Payway.  Log and reconcile online payments with invoicing and bank statements with any reconciliation cycle</p>
                            <small>find out more... </small>
                        </div>
                    </a>
                </div> 
                
                <div class="col-md-3">
                	<a href="/featureoverview/groupportals/" class="link-box" data-toggle="modal" data-target="##modal">
                        <div>
                            <span class="icon-stat"><i class="fas fa-users"></i></span>
                            <h2>Group Portals</h2>
                            <p>Board Members, Committees, Special Interest groups.   You can create your own groups, assign membes, and give them access to a secured Events Calendar, File Repository and Discussion Forum, private for each group you create.</p>
                            <small>find out more... </small>
                        </div>
                    </a>
                </div>
                
                <div class="col-md-3">
                	<a href="/featureoverview/ecommerce/" class="link-box" data-toggle="modal" data-target="##modal">
                        <div>
                            <span class="icon-stat"><i class="fas fa-shopping-cart"></i></span>
                            <h2>eCommerce</h2>
                            <p>Integrated online shop for selling supplies and merchandise.  Easily add products, pricing, bulk discounts and member only prices.  Online payments and Invoicing / Pick & Pack slip generated and emailed in real time.</p>
                            <small>find out more... </small>
                        </div>
                    </a>
                </div>
                
                <div class="col-md-3">
                	<a href="/featureoverview/collaboration/" class="link-box" data-toggle="modal" data-target="##modal">
                        <div>
                            <span class="icon-stat"><i class="fas fa-compress"></i></span>
                            <h2>Collaboration</h2>
                            <p>A broad range of powerful tools to allow Administration, Members, Potential Members, Stakeholders and SIGs to stay informed and share information.  No additional plugins needed, these tools are all built in.</p>
                            <small>find out more... </small>
                        </div>
                    </a>
                </div>
                
            </div>
            
            <div class="row">
                
                <div class="col-md-3">
                	<a href="/featureoverview/cpd/" class="link-box" data-toggle="modal" data-target="##modal">
                        <div>
                            <span class="icon-stat"><i class="fas fa-university"></i></span>
                            <h2>CPD</h2>
                            <p>CPD Logging and monitoring tools fully integrated into members profiles.  Real time calculation eligible points based on Activity Caps.  Administration Monitoring and Auditing tools with great automation features to take the headaches out of CPD.</p>
                            <small>find out more... </small>
                        </div>
                    </a>
                </div>
                
                <div class="col-md-3">
                	<a href="/featureoverview/directory/" class="link-box" data-toggle="modal" data-target="##modal">
                        <div>
                            <span class="icon-stat"><i class="fas fa-search"></i></span>
                            <h2>Directory</h2>
                            <p>If you have members that would like to be found by the public, Membes allows them to opt into a public directory where they can be find by location within radius of anywhere in Australia.  Or if you like, make it Members Only directory resource.</p>
                            <small>find out more... </small>
                        </div>
                    </a>
                </div>
                
                <div class="col-md-3">

                	<a href="/featureoverview/trainingandsupport/" class="link-box" data-toggle="modal" data-target="##modal">
                        <div>
                            <span class="icon-stat"><i class="fas fa-life-ring"></i></span>
                            <h2>Training & Support</h2>
                            <p>Membes with Association Professionals to provide you the best training and support possible.  Association Professionals know Membes, they know Associations and how to ensure you get the most out your future with Membes.</p>
                            <small>find out more... </small>
                        </div>
                    </a>
                </div>
                
                <div class="col-md-3">
                	<a href="/featureoverview/customisable/" class="link-box" data-toggle="modal" data-target="##modal">
                        <div>
                            <span class="icon-stat"><i class="fas fa-sliders-h"></i></span>
                            <h2>Fully Customisable</h2>
                            <p>The base membes platform provides most what an Association needs, but if your needs go beyond what the base platform provides, we can set you up on your very own, allowing you to scope and develop any customised features or functions you may need. </p>
                            <small>find out more... </small>
                        </div>
                    </a>
                </div>
                
            </div>
            
        </div>
    </div>
    
</div>

</cfoutput>