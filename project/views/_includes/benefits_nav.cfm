

<cfscript>

  buttons = [
        {url="/benefits/membership", action="membership", label="Membership"},
        {url="/benefits/events", action="events", label="Event Management"},
        {url="/benefits/finance", action="finance", label="Finance & Payments"},

        {url="/benefits/communication-and-engagement", action="communication-and-engagement", label="Communication"},
        {url="/benefits/education-and-cpd", action="education-and-cpd", label="Education & CPD"},
        {url="/benefits/membership-growth", action="membership-growth", label="Membership Growth"},
        {url="/benefits/executive-and-board", action="executive-and-board", label="Executive & Board"},
        {url="/benefits/latest-technology-standards", action="latest-technology-standards", label="Latest Standards"}

  ];

</cfscript>


<cfoutput>
  <cfloop index="button" array="#buttons#">
      <a href="#button.url#" class="btn <cfif event.getCurrentAction() == button.action>btn-success<cfelse>btn-info</cfif>">#button.label#</a>
  </cfloop>
</cfoutput>

<hr>
