
<style>
    .owl-carousel img {
        max-width: 300px;
    }
    .owl-carousel .item {
        padding-top: 5px;
    }
    #client-logo-mbn {
        padding-top: 24px;
    }
    #client-logo-lymphoedema {
        padding-top: 0px;
    }
    #client-logo-vnca {
        padding-top: 24px;
    }
    #client-logo-omnipathways {
        padding-top: 0px;
    }

    @media (max-width: 1220px) {
	    .owl-carousel .owl-item .item img {
            max-width: 200px;
        }
    }

     @media (max-width: 800px) {
         .owl-carousel {
             /*margin-left: 30px;*/
			 padding-left:30px;
         }
     }

</style>

<cfoutput>

<!-- Recent works -->
<div style="text-align: center">


    <a href="https://www.membes.com.au/about/who-is-using-membes/" class="btn btn-info btn-lg" style="font-size: 24px; width: 650px;">Click Here to see who is using membes</a>


    <!---
    <div class="owl-carousel owl-theme triggerAnimation animated" data-animate="fadeInUp">

    <cfquery name="clients">
        SELECT * FROM org WHERE membes_reference IS NOT NULL
    </cfquery>


    <cfloop query="#clients#">
        <cfif !findNoCase(domain,'membes') and !findNoCase(domain,'adia')>
            <div class="item" style="#membes_reference#">
                <a href="https://#domain#" target="_blank">
                    <img src="https://#domain#/public/#id#/website/logo-head.png">
                </a>
            </div>
        </cfif>
    </cfloop>


        <!---

        <div class="item" style="text-align: center">
             <img src="http://www.hsamembers.org.au/public/27/website/logo-head.png">
        </div>

        <div class="item" style="text-align: center">
             <img src="http://www.acnp.org.au/public/25/website/logo-head.png">
        </div>

        <div class="item" style="text-align: center">
             <img src="http://www.tourismcentralcoast.com/public/34/website/logo-head.png">
        </div>

        <div class="item" style="text-align: center">
             <img src="http://www.ahahypnotherapy.org.au/public/21/website/logo-head.png">
        </div>

        <div class="item" style="text-align: center">
             <img src="http://www.hepatologyassociation.com.au/public/23/website/logo-head.png">
        </div>

        <div class="item" style="text-align: center">
             <img src="http://www.aacuho.edu.au/public/32/website/logo-head.png">
        </div>

        <div class="item" style="text-align: center">
             <img src="http://www.arata.org.au/public/33/website/logo-head.png">
        </div>

    	<div class="item" id="client-logo-sonographers">
            <img src="http://www.sonographers.org/public/12/website/logo-head.png">
        </div>
    	
    	<div class="item" id="client-logo-spasa">
            <img src="http://www.spasa.com.au/public/20/website/logo-head.png">
        </div>
    
        <div class="item">
            <img alt="" src="https://www.renalsociety.org/public/6/website/logo-head.png">
        </div>

        <div class="item">
            <img src="/includes/img/client-logos/adia.png">
        </div>

        <div class="item">
            <img src="https://www.genca.org/public/5/website/logo-head.png">
        </div>

        <div class="item">
            <img src="https://www.mckenzieinstituteaustralia.org/public/8/website/logo-head.png">
        </div>

        <div class="item" id="client-logo-lymphoedema">
            <img src="https://www.lymphoedema.org.au/public/7/website/logo-head.png">
        </div>

        <div class="item">
            <img src="/includes/img/client-logos/abci.png">
        </div>

        <div class="item">
            <img src="https://www.grasslands.org.au/public/10/website/logo-head.png">
        </div>

        <div class="item" id="client-logo-vnca">
            <img src="https://www.vnca.asn.au/public/11/website/logo-head.png">
        </div>

        <div class="item">
             <img src="/includes/img/client-logos/association-professionals.png">
        </div>

        <div class="item" id="client-logo-omnipathways">
            <img src="https://www.omnipathways.org/public/14/website/logo-head.png">
        </div>


        <div class="item" style="text-align: center">
             <img src="https://www.fcia.org.au/public/22/website/logo-head.png">
        </div>

        <div class="item" style="text-align: center">
             <img src="http://www.aibb.org.au/public/19/website/logo-head.png">
        </div>

        <div class="item" id="client-logo-absanz">
            <img src="https://www.absanz.org.au/public/15/website/logo-head.png">
        </div>

        --->


    </div>

    --->

</div>

</cfoutput>