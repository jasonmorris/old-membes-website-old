component{

	function features(event,rc,prc){
		rc.pageTitle = "Membership Software Capabilities";
	}

    function technicaloverview(event,rc,prc){
		rc.pageTitle = "Technical Overview of the membes Association Software Platform";
	}

    function setup(event,rc,prc){
		rc.pageTitle = "membes Association & Membership Management Platform Setup Process";
	}

    function explainervideo(event,rc,prc){
		rc.pageTitle = "Why use membes to manage your Association";
	}

    function mobileapp(event,rc,prc){
		rc.pageTitle = "Engage Your Members with Membes Mobile App";
	}
}