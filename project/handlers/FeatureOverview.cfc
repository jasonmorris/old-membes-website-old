
component extends="coldbox.system.EventHandler"{
	

	any function stakeholdermanagement( event, rc, prc ){
		rc.pageTitle = "Stakeholder and Membership Management";
		rc.modalheading = "<i class='fa fa-user'></i> | Stakeholder Management";
		rc.modalWidth = "wide";
	}

	any function eventmanagement( event, rc, prc ){
		rc.pageTitle = "Event Management";
		rc.modalheading = "<i class='fa fa-calendar'></i> | Event Management";
	}

	any function communications( event, rc, prc ){
		rc.pageTitle = "Communications - Broadcast Email, SMS and MailChimp Integration";
		rc.modalheading = "<i class='fa fa-envelope'></i> | Communications";

	}

	any function financial( event, rc, prc ){
		rc.pageTitle = "Financials - Invoicing and Payments";
		rc.modalheading = "<i class='fa fa-dollar'></i> | Financial";

	}

	any function websitecms( event, rc, prc ){
		rc.pageTitle = "Website Content Management System (CMS)";
		rc.modalheading = "<i class='fa fa-desktop'></i> | Website CMS";

	}

	any function groupportals( event, rc, prc ){
		rc.pageTitle = "Special Interest Group Portals";
		rc.modalheading = "<i class='fa fa-users'></i> | Group Portals";

	}

	any function ecommerce( event, rc, prc ){
		rc.pageTitle = "eCommerce - Online Shop";
		rc.modalheading = "<i class='fa fa-shopping-cart'></i> | e-Commerce";

	}

	any function collaboration( event, rc, prc ){
		rc.pageTitle = "Member and Stakeholder Collaboration";
		rc.modalheading = "<i class='fa fa-compress'></i> | Collaboration";

	}

	any function cpd( event, rc, prc ){
		rc.pageTitle = "CPD Tracking and Auditing";
		rc.modalheading = "<i class='fa fa-university'></i> | CPD";

	}

	any function directory( event, rc, prc ){
		rc.pageTitle = "Public and Private Directories";
		rc.modalheading = "<i class='fa fa-search'></i> | Members Directory";

	}

	any function trainingandsupport( event, rc, prc ){
		rc.pageTitle = "Training and Support";
		rc.modalheading = "<i class='fa fa-life-ring'></i> | Training & Support";

	}

	any function customisable( event, rc, prc ){
		rc.pageTitle = "Configurable Options";
		rc.modalheading = "<i class='fa fa-sliders'></i> | Fully Customisable";

	}	
}