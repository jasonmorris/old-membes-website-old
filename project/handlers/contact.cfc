component{

	property name="mailService" inject="coldbox:plugin:MailService";

	function index(event,rc,prc){
		rc.pageTitle = "Contact membes";
		event.setView('contact/index');
	}
	
	function sendEnquiry(event,rc,prc){

        body = renderView('contact/sendEnquiry');
          
        var email = mailService.newMail(                
        	to = "support@membes.com.au",
        	from = "noreply@membes.com.au",
        	subject="membes - Website Enquiry",
        	type = "html",
        	body = body                
        );        
        	      
        sentEmail = mailService.send(email);

		try{
			//create the service
			var httpService = new http(method = "POST", url = "https://hooks.zapier.com/hooks/catch/3111419/k037g7/silent/");

			//add form params
			httpService.addParam(name = "email", type = "formfield", value = rc.email);

			//send the request
			var result = httpService.send();
		} catch(any e){};

		flash.put('success','Thank you for your enquiry.  We will get back to you shortly.');   
        setNextEvent('contact?sent');
		
	}
	
}