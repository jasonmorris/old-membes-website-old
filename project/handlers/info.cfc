
component extends="coldbox.system.EventHandler"{
	

	function article1( event, rc, prc ){
		rc.pageTitle = "How an AMS is more than just a CRM";
		rc.metaDescription = "A CRM is a 'CUSTOMER Relationship Management' System and is designed to help organisations manage their 'Customers'. Whereas an AMS 'ASSOCIATION Management System' is designed to help Associations manage all aspects of the Association.";
		rc.metaKeywords = 'AMS, CRM, Association Management Software, Customer Relationship Management, Association Software, Membership Management, Membership Software, NFP Software, Cloud Membership';
		renderview('info/article1');
	}	
	function article2( event, rc, prc ){
		rc.pageTitle = "Bespoke vs. SaaS Association Management Software";
		rc.metaDescription = "An Association Management Software (AMS) platform is designed to significantly improve administration efficiency, reduce manual processing and improve data insights for most associations or member-based organisations. However, in selecting an AMS, you have two paths you can take - bespoke or modern SaaS. Which is right for you?";
		rc.metaKeywords = 'Membership SaaS, Members SaaS, SaaS AMS, SaaS, AMS, Bespoke, Custom Software, Association Software, Association Management';
	}
	function article3( event, rc, prc ){
		rc.pageTitle = "Importance of an Open Ecosystem over Closed Ecosystem AMS";
		rc.metaDescription = "Increasingly Association Management Software (AMS) is becoming a critical part of the day to day management and growth of an Association. Modern AMS takes the manual work that Associations traditionally did manually and automates them. From membership joining, renewals, communications to training, eCommerce and website management.";
		rc.metaKeywords = 'AMS, Membership Software, ecosystem, eco-system, Membership, Association Software, Association Management';
	}
	function article4( event, rc, prc ){
		rc.pageTitle = "Membes Design Flexibility";
		rc.metaDescription = "Membes Association Management Software (AMS) platform comes with the flexibility to work with existing website CMS platform, connect and share data to mobile Apps or 3rd party software via its API, and also comes with a powerful integrated CMS.";
		rc.metaKeywords = 'Membes Design, Association Software Design, AMS Design, Association Design, AMS CMS, Association CMS, Member CMS, Members CMS, Membership CMS';
	}
	function article5( event, rc, prc ){
		rc.pageTitle = "Association Software Pitfalls - Making the Right Choice";
		rc.metaDescription = "Software is a tool, and like many tools, you need to select the right one for the task at hand. Bespoke software, which has been the traditional approach for many years for associations building a CRM, database or website solution.";
		rc.metaKeywords = '';
	}
	function article6( event, rc, prc ){
		rc.pageTitle = "Engage with your Members";
		rc.metaDescription = "Many organisations seem like they’ve only just crawled out of the dark ages by moving away from spreadsheets to now, perhaps something a little more automated and cloud-based. Are you making use of modern technology and software to engage with your members?";
		rc.metaKeywords = '';
	}

}