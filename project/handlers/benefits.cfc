component{

	function saasbenefits(event,rc,prc){
		rc.pageTitle = "Benefits of Association Management Software (AMS) for Associations & NFPs";
        event.setView('benefits/index');
	}

    function fortheboard(event,rc,prc){
		rc.pageTitle = "Membes Benefits for the Board";
        event.setView('benefits/for-the-board');
	}

    function executivebenefits(event,rc,prc){
		rc.pageTitle = "Membes Benefits for Executives";
        event.setView('benefits/for-executives');
	}

    function staffbenefits(event,rc,prc){
		rc.pageTitle = "Membes Benefits for Staff - Admininstrators, Membership, Events, Communications Officers";
        event.setView('benefits/for-staff');
	}
}