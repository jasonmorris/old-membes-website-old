component{

	property name="mailService" inject="coldbox:plugin:MailService";

	function index(event,rc,prc){
		rc.pageTitle = "Request a Demo of the membes: Association Software";
		event.setView('demo/index');
	}
	
	function sendDemoRequest(event,rc,prc){
	    
        body = renderView('contact/sendDemoRequest');
          
        var email = mailService.newMail(                
        	to = "support@membes.com.au",
        	from = "noreply@membes.com.au",
        	subject="membes - Request for Demo",
        	type = "html",
        	body = body                
        );        
        	      
        sentEmail = mailService.send(email);
		
		//create the service
		var httpService = new http(method = "POST", url = "https://hooks.zapier.com/hooks/catch/3111419/k037g7/silent/");

		//add form params
		httpService.addParam(name = "email", type = "formfield", value = rc.email);

		//send the request
		var result = httpService.send();

		flash.put('success','Thank you for your request.  We will get back to you shortly to arrange a time that suits you.');
        setNextEvent('demo?sent');


		
		
	}
	
}