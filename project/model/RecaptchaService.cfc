/**
* I am a new Model Object
*/
component singleton accessors="true"{

	/**
	 * Constructor
	 */
	RecaptchaService function init(){
		this.verifyUrl = 'https://www.google.com/recaptcha/api/siteverify';
		this.secret = '6LfjzrkUAAAAAFctWOzwBE2jOy3NKbL6omiMnvM8';
		this.siteKey = "6LfjzrkUAAAAAAMcoUud-GQNrCLnlD0MXLkOjVq0";
		this.scoreThreshold = 0.5;
		return this;
	}

	function getSiteKey(){
		return this.siteKey;
	}

	function verifyV3(required token, required action){
		try{
			var httpService = new http(method = "POST", url = this.verifyUrl);
			httpService.addParam(name = "secret", type = "formfield", value = this.secret);
			httpService.addParam(name = "response", type = "formfield", value = token);
			var result = deserializeJson(httpService.send().getPrefix().filecontent);
			return (result.success && result.action == action && result.score >= this.scoreThreshold);

		} catch(any e){// failure consider false
			return false;
 		}
	}
}
