component extends="coldbox.system.coldbox"{

    this.sessionManagement = true;
    this.setClientCookies = true;

    if ( structKeyExists(url, "healthcheck") AND url.healthcheck eq "live" ){
        healthcheckLive();
    }

    system = createObject("java", "java.lang.System");

    this.datasource       = "membes";
    if ("" & system.getEnv("MEMBES_DSN_USERNAME") neq "") {
        this.datasources.membes_website = {
            "class" = system.getEnv("MEMBES_DSN_CLASS"),
            "connectionString" = system.getEnv("MEMBES_DSN_CONNECTIONSTRING"),
            "database" = system.getEnv("MEMBES_DSN_DATABASE"),
            "driver" = system.getEnv("MEMBES_DSN_DRIVER"),
            "host" = system.getEnv("MEMBES_DSN_HOST"),
            "port" = system.getEnv("MEMBES_DSN_PORT"),
            "type" = system.getEnv("MEMBES_DSN_TYPE"),
            "url" = system.getEnv("MEMBES_DSN_URL"),
            "username" = system.getEnv("MEMBES_DSN_USERNAME"),
            "password" = system.getEnv("MEMBES_DSN_PASSWORD")
        };
        this.datasource       = "membes_website";
    }

    if ("" & system.getEnv("SMTP_SERVER") neq "") {
        this.tag.mail.server = system.getEnv("SMTP_SERVER");
        this.tag.mail.port = system.getEnv("SMTP_PORT");
        this.tag.mail.username = system.getEnv("SMTP_USERNAME");
        this.tag.mail.password = system.getEnv("SMTP_PASSWORD");
    }

    if(structKeyExists(server.system.environment, "LUCEE_SESSION_MEMCACHED_SERVERS")){
        this.cache.connections["sessions"] = {
            class: 'org.lucee.extension.cache.mc.MemcachedCache'
            , bundleName: 'memcached.extension'
            , bundleVersion: '4.0.0.7-SNAPSHOT'
            , storage: true
            , custom: {
                "socket_timeout":"30",
                "initial_connections":"1",
                "alive_check":"true",
                "buffer_size":"1",
                "max_spare_connections":"32",
                "storage_format":"Binary",
                "socket_connect_to":"3",
                "min_spare_connections":"1",
                "maint_thread_sleep":"5",
                "failback":"true",
                "max_idle_time":"600",
                "max_busy_time":"30",
                "nagle_alg":"true",
                "failover":"true",
                "servers": server.system.environment["LUCEE_SESSION_MEMCACHED_SERVERS"] ?: ""
            }
            , default: ''
        };
        this.sessioncluster = server.system.environment["LUCEE_APPLICATION_SESSIONCLUSTER"] ?: "false";
        this.sessionstorage = server.system.environment["LUCEE_SESSION_STORE"] ?: "memory";
    }

    // Mappings Imports
    import coldbox.system.*;
    
    // COLDBOX STATIC PROPERTY, DO NOT CHANGE UNLESS THIS IS NOT THE ROOT OF YOUR COLDBOX APP
    COLDBOX_APP_ROOT_PATH = getDirectoryFromPath(getCurrentTemplatePath());
    // The web server mapping to this application. Used for remote purposes or static purposes
    COLDBOX_APP_MAPPING   = "";
    // COLDBOX PROPERTIES
    COLDBOX_CONFIG_FILE      = "";
    // COLDBOX APPLICATION KEY OVERRIDE
    COLDBOX_APP_KEY          = "";

    // request start
    public boolean function onRequestStart(String targetPage){
        if ( structKeyExists(url, "healthcheck") AND url.healthcheck eq "ready" ){
            healthcheckReady();
        }

        return super.onRequestStart( arguments.targetPage );
    }

    // health check to test if application is loaded and ready (from rancher)
    private function healthcheckReady(){
        if ( !structKeyExists(application,"cbBootstrap") ){
            cfheader(statuscode="503" statustext="Unavailable: Application has not started");
            abort;
        }
        cfheader(statuscode="200" statustext="OK");
        abort;
    }

    // health check to test if appliation is live (from rancher)
    private function healthcheckLive(){
        cfheader(statuscode="200" statustext="OK");
        abort;
    }
}
