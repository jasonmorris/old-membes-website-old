FROM lucee/lucee:5.3.8.149-SNAPSHOT-nginx

ENV LUCEE_JAVA_OPTS "-Xms512m -Xmx1024m"

ENV LUCEE_EXTENSIONS "FAD1E8CB-4F45-4184-86359145767C29DE;name=Hibernate ORM Engine,EFDEB172-F52E-4D84-9CD1A1F561B3DFC8;name=lucene Search Engine"
ENV LUCEE_SESSION_STORE "memory"
ENV LUCEE_APPLICATION_SESSIONCLUSTER "false"
ENV TZ "Australia/Sydney"

# CSV tag
RUN wget -O /tmp/extension.zip https://github.com/paulklinkenberg/lucee-cfcsv/raw/master/dist/classic/extension.zip && \
    unzip /tmp/extension.zip -d /tmp/extension && \
    unzip /tmp/extension/thecode.zip -d /tmp/thecode && \
    cp -R /tmp/thecode/* /opt/lucee/server/lucee-server/context/library/tag/ && \
    rm /tmp/extension.zip && \
    rm -rf /tmp/extension && \
    rm -rf /tmp/thecode

# Install fusion reactor
RUN mkdir -p /opt/fusionreactor/
ADD https://intergral-dl.s3.amazonaws.com/FR/Latest/fusionreactor.jar /opt/fusionreactor/fusionreactor.jar
ADD https://intergral-dl.s3.amazonaws.com/FR/Latest/libfrjvmti_x64.so /opt/fusionreactor/libfrjvmti_x64.so

# NGINX configs
COPY config/nginx/ /etc/nginx/

# Lucee server configs
COPY config/lucee/lucee-web.xml.cfm /opt/lucee/web/lucee-web.xml.cfm
COPY config/lucee/lucee-server.xml /opt/lucee/server/lucee-server/context/lucee-server.xml

ADD https://ext.lucee.org/memcached-extension-4.0.0.7-SNAPSHOT.lex /opt/lucee/server/lucee-server/deploy/
RUN /usr/local/tomcat/bin/prewarm.sh && /usr/local/tomcat/bin/prewarm.sh

# Deploy codebase to container
COPY project /var/www

#customise the run time directory structure
RUN mkdir -p /var/www/_temp